#ifndef LJcutAnalysis_h
#define LJcutAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

#include <vector>
#include <map>
#include <string>
#include "TH1D.h"
#include "TH2F.h"
#include <TMath.h>

class LJcutAnalysis : public TSelector {
    public :
    TTreeReader fReader; //The tree reader
    TTree *fChain = 0; //Pointer to ther analyzed TTree or TChain

    //Options passed by Run.py
    bool doCuts;

    // Readers to access the data
    TTreeReaderValue<Bool_t> isMC = {fReader, "isMC"};
    // TTreeReaderValue<Int_t> lead_index = {fReader, "lead_index"};
    // TTreeReaderValue<Int_t> far_index = {fReader, "far_index"};
    int *lead_index = new int(-1);
    int *far_index = new int(-1);
    TTreeReaderArray<int> types = {fReader, "types"};
    TTreeReaderArray<int> LJmuon_index = {fReader, "LJmuon_index"};
    TTreeReaderArray<int> LJmuon_author = {fReader, "LJmuon_author"};
    TTreeReaderArray<int> LJmuon_type = {fReader, "LJmuon_type"};
    TTreeReaderArray<double> LJmuon_RPCtime = {fReader, "LJmuon_RPCtime"};
    TTreeReaderArray<float> LJmuon_eta = {fReader, "LJmuon_eta"};
    TTreeReaderArray<float> LJmuon_phi = {fReader, "LJmuon_phi"};
    TTreeReaderArray<float> LJmuon_pt = {fReader, "LJmuon_pt"};
    TTreeReaderValue<Int_t> eventNumber = {fReader, "eventNumber"};

    // User defined variables
    TFile *outFile = nullptr;
    int eventCounter = 0;
    int nTotalEntries = 0;
    float intLumi = 0;

    //cuuts for 0-0 and 2-2 channel (no utilizado todavia)

    // For microT-2muDPJ (creo que tree de salida)
    TTree * microT_00 = 0;
    //vector<bool> u_isMC;
    //vector<float> u_LJmuon_isLeading;
    vector<float> u_LJmuon_eta;
    vector<float> u_LJmuon_phi;
    vector<float> u_LJmuon_pt;
    //vector<float> u_LJmuon_z0;
    vector<float> u_LJmuon_RPCtime;
    //vector<int>   u_LJmuon_RPCinfo;
    //vector<float>   u_LJmuon_NNscore;
    vector<int>   u_LJmuon_author;
    vector<int>   u_LJmuon_type;

    //Bools cortes
    bool muonEtaCut = true;

    //Funciones
    LJcutAnalysis(TTree * /*tree*/=0) { }
    virtual ~LJcutAnalysis() { }
    virtual void Begin(TTree *tree);
    virtual void SlaveBegin(TTree *tree);
    virtual Bool_t Process(Long64_t entry);
    virtual Int_t GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
    virtual void SlaveTerminate();
    virtual void Terminate();
    virtual void FillMicroT00();
    //virtual void FillhLJTree(); //TTree for CaloCluster studies and jet images production 
    virtual void    SetInputList(TList *input) { fInput = input; }
    virtual TList  *GetOutputList() const { return fOutput; }
    virtual void    SetOption(const char *option) { fOption = option; }
    virtual void    Init(TTree *tree);
    virtual Bool_t  Notify();
    virtual void    LJstudy();

    ClassDef(LJcutAnalysis,0);
};

#endif
/*
#ifdef LJcutAnalysis_cxx

void LJcutAnalysis::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the reader is initialized.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  fReader.SetTree(tree);
}

Bool_t LJcutAnalysis::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}
*/
//void LJcutAnalysis::SlaveBegin(TTree * /*tree*/)
//{
//  TString option = GetOption();
//}
//
//#endif