#!/usr/bin/python
from __future__ import print_function
import argparse
import sys
import os
import ROOT
from collections import OrderedDict
#import matplotlib.pyplot as plt
ROOT.gROOT.SetBatch(True)
### Mini Ntuple
def getOptions(args=sys.argv[1:]):
    ###  Argument parser ###
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("-i", "--input", 
                        help="Input files (either comma-separated .root files or .txt list of files)", 
                        type=str,
                        nargs="+")
    parser.add_argument("-o", "--output", 
                        help="Output file name (default is outfile) - .root is added if not present", 
                        default="outfile", 
                        type=str)
    parser.add_argument("-T","--fullNtuple", 
                        help="Run on T instead of miniT", 
                        action="store_true")
    parser.add_argument("--mode", 
                        help="to run specific cutflow",
                        default="Standard",
                        type=str)
    parser.add_argument("--doValidationTrees",
                        help="avoid producing validation trees (abcd and microT_XX)",
                        dest="doValidTrees",
                        default="0",
                        action="store_const",
                        const="1")
    parser.add_argument("--doLiRe",
                        help="compute curves for lifetime reweighting",
                        default="0",
                        action="store_const",
                        const="1")
    parser.add_argument("--doCalRatioSyst",
                        help="Use HLT emulated calRatio trigger",
                        default="no",
                        type=str)
    parser.add_argument("--truthHiggsPtReweighting",
                        help="apply higher order reweighting to the truth higgs pt",
                        default="no",
                        action="store_const",
                        const="yes"
                        )
    parser.add_argument("--tree",
                        default="miniT",
                        type=str)
                                                
    options = parser.parse_args(args)
    return options

def getSig(Nevt, Nbkg):
    #if Nbkg == 0:
    #    return 0
    #return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(Nevt, Nbkg, 0.2)
    if Nevt + Nbkg == 0:
        return 0
    return Nevt / (Nevt + Nbkg) ** 0.5 #simple para verificar

def RunSampleComparison(SampleInputs, SampleOutputs):
    options = getOptions(sys.argv[1:])
    for process in SampleInputs.keys():
        if not SampleInputs[process]:
            continue
        print('Processing {}'.format(process))
        ch = ROOT.TChain('miniT')
        for file in SampleInputs[process]:
                ch.Add(file)
        nTotalEntries = ch.GetEntries()
        arguments = [SampleOutputs[process],
                    str(nTotalEntries),
                    options.mode,
                    options.doValidTrees,
                    options.doLiRe,
                    options.doCalRatioSyst,
                    options.truthHiggsPtReweighting]
        argumentsStr = " ".join(arguments)
        SC = ch.Process("LJexplorer.C+",argumentsStr)
    #LOAD FILES AND COUNT EVENTS
    Cuts00 = list()
    HAHM1WEvents00 = list()
    HAHM2WEvents00 = list()
    HAHM3WEvents00 = list()
    HAHM4WEvents00 = list()
    HAHM5WEvents00 = list()
    FRVZ1WEvents00 = list()
    FRVZ2WEvents00 = list()
    FRVZ3WEvents00 = list()
    FRVZ4WEvents00 = list()
    FRVZ5WEvents00 = list()
    Cuts02 = list()
    HAHM1WEvents02 = list()
    HAHM2WEvents02 = list()
    HAHM3WEvents02 = list()
    HAHM4WEvents02 = list()
    HAHM5WEvents02 = list()
    FRVZ1WEvents02 = list()
    FRVZ2WEvents02 = list()
    FRVZ3WEvents02 = list()
    FRVZ4WEvents02 = list()
    FRVZ5WEvents02 = list()
    Cuts22 = list()
    HAHM1WEvents22 = list()
    HAHM2WEvents22 = list()
    HAHM3WEvents22 = list()
    HAHM4WEvents22 = list()
    HAHM5WEvents22 = list()
    FRVZ1WEvents22 = list()
    FRVZ2WEvents22 = list()
    FRVZ3WEvents22 = list()
    FRVZ4WEvents22 = list()
    FRVZ5WEvents22 = list()

    fh1 = ROOT.TFile.Open(SampleOutputs['HAHM1'], "READ")
    H1WEvt00 = fh1.CutWEvents00
    H1WEvt02 = fh1.CutWEvents02
    H1WEvt22 = fh1.CutWEvents22
    i = 0
    while i < H1WEvt00.GetNbinsX():
        Cuts00.append(H1WEvt00.GetXaxis().GetBinLabel(i+1))
        HAHM1WEvents00.append(int(H1WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H1WEvt02.GetNbinsX():
        Cuts02.append(H1WEvt02.GetXaxis().GetBinLabel(i+1))
        HAHM1WEvents02.append(int(H1WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H1WEvt22.GetNbinsX():
        Cuts22.append(H1WEvt22.GetXaxis().GetBinLabel(i+1))
        HAHM1WEvents22.append(int(H1WEvt02.GetBinContent(i+1)))
        i += 1
    fh2 = ROOT.TFile.Open(SampleOutputs['HAHM2'], "READ")
    H2WEvt00 = fh2.CutWEvents00
    H2WEvt02 = fh2.CutWEvents02
    H2WEvt22 = fh2.CutWEvents22
    i = 0
    while i < H2WEvt00.GetNbinsX():
        HAHM2WEvents00.append(int(H2WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H2WEvt02.GetNbinsX():
        HAHM2WEvents02.append(int(H2WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H2WEvt22.GetNbinsX():
        HAHM2WEvents22.append(int(H2WEvt02.GetBinContent(i+1)))
        i += 1
    fh3 = ROOT.TFile.Open(SampleOutputs['HAHM3'], "READ")
    H3WEvt00 = fh3.CutWEvents00
    H3WEvt02 = fh3.CutWEvents02
    H3WEvt22 = fh3.CutWEvents22
    i = 0
    while i < H3WEvt00.GetNbinsX():
        HAHM3WEvents00.append(int(H3WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H3WEvt02.GetNbinsX():
        HAHM3WEvents02.append(int(H3WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H3WEvt22.GetNbinsX():
        HAHM3WEvents22.append(int(H3WEvt02.GetBinContent(i+1)))
        i += 1
    fh4 = ROOT.TFile.Open(SampleOutputs['HAHM4'], "READ")
    H4WEvt00 = fh4.CutWEvents00
    H4WEvt02 = fh4.CutWEvents02
    H4WEvt22 = fh4.CutWEvents22
    i = 0
    while i < H4WEvt00.GetNbinsX():
        HAHM4WEvents00.append(int(H4WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H4WEvt02.GetNbinsX():
        HAHM4WEvents02.append(int(H4WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H4WEvt22.GetNbinsX():
        HAHM4WEvents22.append(int(H4WEvt02.GetBinContent(i+1)))
        i += 1
    fh5 = ROOT.TFile.Open(SampleOutputs['HAHM5'], "READ")
    H5WEvt00 = fh5.CutWEvents00
    H5WEvt02 = fh5.CutWEvents02
    H5WEvt22 = fh5.CutWEvents22
    i = 0
    while i < H5WEvt00.GetNbinsX():
        HAHM5WEvents00.append(int(H5WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H5WEvt02.GetNbinsX():
        HAHM5WEvents02.append(int(H5WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < H5WEvt22.GetNbinsX():
        HAHM5WEvents22.append(int(H5WEvt02.GetBinContent(i+1)))
        i += 1
    #FRVZ
    ff1 = ROOT.TFile.Open(SampleOutputs['FRVZ1'], "READ")
    F1WEvt00 = ff1.CutWEvents00
    F1WEvt02 = ff1.CutWEvents02
    F1WEvt22 = ff1.CutWEvents22
    i = 0
    while i < F1WEvt00.GetNbinsX():
        FRVZ1WEvents00.append(int(F1WEvt00.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F1WEvt02.GetNbinsX():
        FRVZ1WEvents02.append(int(F1WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F1WEvt22.GetNbinsX():
        FRVZ1WEvents22.append(int(F1WEvt22.GetBinContent(i+1)))
        i += 1
    ff2 = ROOT.TFile.Open(SampleOutputs['FRVZ2'], "READ")
    F2WEvt00 = ff2.CutWEvents00
    F2WEvt02 = ff2.CutWEvents02
    F2WEvt22 = ff2.CutWEvents22
    i = 0
    while i < F2WEvt00.GetNbinsX():
        FRVZ2WEvents00.append(int(F2WEvt00.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F2WEvt02.GetNbinsX():
        FRVZ2WEvents02.append(int(F2WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F2WEvt22.GetNbinsX():
        FRVZ2WEvents22.append(int(F2WEvt22.GetBinContent(i+1)))
        i += 1
    ff3 = ROOT.TFile.Open(SampleOutputs['FRVZ3'], "READ")
    F3WEvt00 = ff3.CutWEvents00
    F3WEvt02 = ff3.CutWEvents02
    F3WEvt22 = ff3.CutWEvents22
    i = 0
    while i < F3WEvt00.GetNbinsX():
        FRVZ3WEvents00.append(int(F3WEvt00.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F3WEvt02.GetNbinsX():
        FRVZ3WEvents02.append(int(F3WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F3WEvt22.GetNbinsX():
        FRVZ3WEvents22.append(int(F3WEvt22.GetBinContent(i+1)))
        i += 1
    ff4 = ROOT.TFile.Open(SampleOutputs['FRVZ4'], "READ")
    F4WEvt00 = ff4.CutWEvents00
    F4WEvt02 = ff4.CutWEvents02
    F4WEvt22 = ff4.CutWEvents22
    i = 0
    while i < F4WEvt00.GetNbinsX():
        FRVZ4WEvents00.append(int(F4WEvt00.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F4WEvt02.GetNbinsX():
        FRVZ4WEvents02.append(int(F4WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F4WEvt22.GetNbinsX():
        FRVZ4WEvents22.append(int(F4WEvt22.GetBinContent(i+1)))
        i += 1
    ff5 = ROOT.TFile.Open(SampleOutputs['FRVZ5'], "READ")
    F5WEvt00 = ff5.CutWEvents00
    F5WEvt02 = ff5.CutWEvents02
    F5WEvt22 = ff5.CutWEvents22
    i = 0
    while i < F5WEvt00.GetNbinsX():
        FRVZ5WEvents00.append(int(F5WEvt00.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F5WEvt02.GetNbinsX():
        FRVZ5WEvents02.append(int(F5WEvt02.GetBinContent(i+1)))
        i += 1
    i = 0
    while i < F5WEvt22.GetNbinsX():
        FRVZ5WEvents22.append(int(F5WEvt22.GetBinContent(i+1)))
        i += 1
    print('\n\n 0-0 \n\n')
    print('#Cuts 0-0')
    print('Cuts00 = ', Cuts00)
    print('HAHM1WEvents00 = ', HAHM1WEvents00)
    print('HAHM2WEvents00 = ', HAHM2WEvents00)
    print('HAHM3WEvents00 = ', HAHM3WEvents00)
    print('HAHM4WEvents00 = ', HAHM4WEvents00)
    print('HAHM5WEvents00 = ', HAHM5WEvents00)
    print('FRVZ1WEvents00 = ', FRVZ1WEvents00)
    print('FRVZ2WEvents00 = ', FRVZ2WEvents00)
    print('FRVZ3WEvents00 = ', FRVZ3WEvents00)
    print('FRVZ4WEvents00 = ', FRVZ4WEvents00)
    print('FRVZ5WEvents00 = ', FRVZ5WEvents00)
    print('\n\n 0-2 \n\n')
    print('Cuts02 = ', Cuts02)
    print('HAHM1WEvents02 = ', HAHM1WEvents02)
    print('HAHM2WEvents02 = ', HAHM2WEvents02)
    print('HAHM3WEvents02 = ', HAHM3WEvents02)
    print('HAHM4WEvents02 = ', HAHM4WEvents02)
    print('HAHM5WEvents02 = ', HAHM5WEvents02)
    print('FRVZ1WEvents02 = ', FRVZ1WEvents02)
    print('FRVZ2WEvents02 = ', FRVZ2WEvents02)
    print('FRVZ3WEvents02 = ', FRVZ3WEvents02)
    print('FRVZ4WEvents02 = ', FRVZ4WEvents02)
    print('FRVZ5WEvents02 = ', FRVZ5WEvents02)
    print('\n\n 2-2 \n\n')
    print('Cuts22 = ', Cuts22)
    print('HAHM1WEvents22 = ', HAHM1WEvents22)
    print('HAHM2WEvents22 = ', HAHM2WEvents22)
    print('HAHM3WEvents22 = ', HAHM3WEvents22)
    print('HAHM4WEvents22 = ', HAHM4WEvents22)
    print('HAHM5WEvents22 = ', HAHM5WEvents22)
    print('FRVZ1WEvents22 = ', FRVZ1WEvents22)
    print('FRVZ2WEvents22 = ', FRVZ2WEvents22)
    print('FRVZ3WEvents22 = ', FRVZ3WEvents22)
    print('FRVZ4WEvents22 = ', FRVZ4WEvents22)
    print('FRVZ5WEvents22 = ', FRVZ5WEvents22)



def RunFullAnalysis(inputFiles, outputFiles):
    for process in inputFiles.keys():
        if not inputFiles[process]:
            continue
        if type(inputFiles[process]) == list:
            print("Processing {}".format(process))
            options = getOptions(sys.argv[1:])
            ch = ROOT.TChain('miniT')
            for file in inputFiles[process]:
                ch.Add(file)
                print('Loading: {}'.format(file))
            nTotalEntries = ch.GetEntries()
            arguments = [outputFiles[process],
                        str(nTotalEntries),
                        options.mode,
                        options.doValidTrees,
                        options.doLiRe,
                        options.doCalRatioSyst,
                        options.truthHiggsPtReweighting]
            argumentsStr = " ".join(arguments)
            SC = ch.Process("LJexplorer.C+",argumentsStr)

def RunHistStudy00(outputFiles):
    #Conteo de eventos
    outEvents = OrderedDict()
    outEventsW = OrderedDict()
    outDNNW = OrderedDict()
    for out in outputFiles.keys():
        processEvents = OrderedDict()
        processEventsW = OrderedDict()
        processDNNW= OrderedDict()
        f = ROOT.TFile.Open(outputFiles[out], "READ")
        hEventsW = f.CutWEvents00
        hEvents = f.CutEvents00
        i = 0
        while i < hEvents.GetNbinsX():
            processEvents[hEvents.GetXaxis().GetBinLabel(i+1)] = int(hEvents.GetBinContent(i+1))
            processEventsW[hEventsW.GetXaxis().GetBinLabel(i+1)] = int(hEventsW.GetBinContent(i+1))
            i += 1
        outEvents[out] = processEvents
        outEventsW[out] = processEventsW
        #DNN
        hDNN = f.histMuonDNN00
        i = 0
        while i < hDNN.GetNbinsX():
            processDNNW[hDNN.GetXaxis().GetBinLabel(i+1)] = int(hDNN.GetBinContent(i+1))
            i += 1
        outDNNW[out] = processDNNW

    #imprimimos resultados
    Cuts00 = list()
    EventsFRVZ = list()
    EventsWFRVZ = list()
    EventsHAHM = list()
    EventsWHAHM = list()
    CutsDNN00 = list()
    dnnFRVZ00 = list()
    WdnnFRVZ00 = list()
    dnnHAHM00 = list()
    WdnnHAHM00 = list()
    for cut in outEvents['FRVZ'].keys():
        Cuts00.append(cut)
        EventsFRVZ.append(outEvents['FRVZ'][cut])
        EventsWFRVZ.append(outEventsW['FRVZ'][cut])
        EventsHAHM.append(outEvents['HAHM'][cut])
        EventsWHAHM.append(outEventsW["HAHM"][cut])
    for cut in outDNNW['FRVZ'].keys():
        CutsDNN00.append(float(cut))
        WdnnFRVZ00.append(outDNNW['FRVZ'][cut])
        WdnnHAHM00.append(outDNNW['HAHM'][cut])
    print("\n 0-0 \n\n")
    print('#Events')
    print('Cuts00 = ', Cuts00)
    print('EventsFRVZ00 = ', EventsFRVZ)
    print('WEventsFRVZ00 = ', EventsWFRVZ)
    print('EventsHAHM00 = ', EventsHAHM)
    print('WEventsHAHM00 = ', EventsWHAHM)
    print('#DNN')
    print('CutsDNN00 = ', CutsDNN00)
    print('WdnnFRVZ00 = ', WdnnFRVZ00)
    print('WdnnHAHM00 = ', WdnnHAHM00)



def RunHistStudy02(outputFiles):
    #Conteo de eventos
    outEvents = OrderedDict()
    outEventsW = OrderedDict()
    outDNNW = OrderedDict()
    outDPhiW = OrderedDict()
    outGapRatioW = OrderedDict()
    outTimingW = OrderedDict()
    outBIBtaggerW = OrderedDict()
    for out in outputFiles.keys():
        processEvents = OrderedDict()
        processEventsW = OrderedDict()
        processDNNW = OrderedDict()
        processDPhiW = OrderedDict()
        processGapRatioW = OrderedDict()
        processTimingW = OrderedDict()
        processBIBtaggerW = OrderedDict()
        f = ROOT.TFile.Open(outputFiles[out], "READ")
        #Events
        hEventsW = f.CutWEvents02
        hEvents = f.CutEvents02
        i = 0
        while i < hEvents.GetNbinsX():
            processEvents[hEvents.GetXaxis().GetBinLabel(i+1)] = int(hEvents.GetBinContent(i+1))
            processEventsW[hEventsW.GetXaxis().GetBinLabel(i+1)] = int(hEventsW.GetBinContent(i+1))
            i += 1
        outEvents[out] = processEvents
        outEventsW[out] = processEventsW
        #DNN
        hDNN = f.histMuonDNN02
        i = 0
        while i < hDNN.GetNbinsX():
            processDNNW[hDNN.GetXaxis().GetBinLabel(i+1)] = int(hDNN.GetBinContent(i+1))
            i += 1
        outDNNW[out] = processDNNW
        #DPhi
        hDPhi = f.histDeltaPhi02
        i = 0
        while i < hDPhi.GetNbinsX():
            processDPhiW[hDPhi.GetXaxis().GetBinLabel(i+1)] = int(hDPhi.GetBinContent(i+1))
            i += 1
        outDPhiW[out] = processDPhiW
        #GapRatio
        hGapRatio =  f.histGapRatio02
        i = 0
        while i < hGapRatio.GetNbinsX():
            processGapRatioW[hGapRatio.GetXaxis().GetBinLabel(i+1)] = int(hGapRatio.GetBinContent(i+1))
            i += 1
        outGapRatioW[out] = processGapRatioW
        #Timing
        hTiming = f.histTiming02
        i = 0
        while i < hTiming.GetNbinsX():
            processTimingW[hTiming.GetXaxis().GetBinLabel(i+1)] = int(hTiming.GetBinContent(i+1))
            i += 1
        outTimingW[out] = processTimingW
        #BIBtagger
        hBIB = f.histBIBtagger02
        i = 0
        while i < hBIB.GetNbinsX():
            processBIBtaggerW[hBIB.GetXaxis().GetBinLabel(i+1)]  = int(hBIB.GetBinContent(i+1))
            i += 1
        outBIBtaggerW[out] = processBIBtaggerW
    #imprimimos resultados
    Cuts02 = list()
    EventsFRVZ = list()
    EventsWFRVZ = list()
    EventsHAHM = list()
    EventsWHAHM = list()
    CutsDNN02 = list()
    dnnFRVZ02 = list()
    WdnnFRVZ02 = list()
    dnnHAHM02 = list()
    WdnnHAHM02 = list()
    CutsDPhi02 = list()
    DPhiFRVZ02 = list()
    WDPhiFRVZ02 = list()
    DPhiHAHM02 = list()
    WDPhiHAHM02 =  list()
    CutsGapRatio02 = list()
    GapRatioFRVZ02 = list()
    WGapRatioFRVZ02 = list()
    GapRatioHAHM02 = list()
    WGapRatioHAHM02 =  list()
    CutsTiming02 = list()
    TimingFRVZ02 = list()
    WTimingFRVZ02 = list()
    TimingHAHM02 = list()
    WTimingHAHM02 =list()
    CutsBIBtagger02 = list()
    BIBtaggerFRVZ02 = list()
    WBIBtaggerFRVZ02 =  list()
    BIBtaggerHAHM02 = list()
    WBIBtaggerHAHM02 = list()
    for cut in outEvents['FRVZ'].keys():
        Cuts02.append(cut)
        EventsFRVZ.append(outEvents['FRVZ'][cut])
        EventsWFRVZ.append(outEventsW['FRVZ'][cut])
        EventsHAHM.append(outEvents['HAHM'][cut])
        EventsWHAHM.append(outEventsW["HAHM"][cut])
    for cut in outDNNW['FRVZ'].keys():
        CutsDNN02.append(float(cut))
        WdnnFRVZ02.append(outDNNW['FRVZ'][cut])
        WdnnHAHM02.append(outDNNW['HAHM'][cut])
    for cut in outDPhiW['FRVZ'].keys():
        CutsDPhi02.append(float(cut))
        WDPhiFRVZ02.append(outDPhiW['FRVZ'][cut])
        WDPhiHAHM02.append(outDPhiW['HAHM'][cut])
    for  cut in outGapRatioW['FRVZ'].keys():
        CutsGapRatio02.append(float(cut))
        WGapRatioFRVZ02.append(outGapRatioW['FRVZ'][cut])
        WGapRatioHAHM02.append(outGapRatioW['HAHM'][cut])
    for cut in outTimingW['FRVZ'].keys():
        CutsTiming02.append(float(cut))
        WTimingFRVZ02.append(outTimingW['FRVZ'][cut])
        WTimingHAHM02.append(outTimingW['HAHM'][cut])
    for cut in outBIBtaggerW['FRVZ'].keys():
        CutsBIBtagger02.append(float(cut))
        WBIBtaggerFRVZ02.append(outBIBtaggerW['FRVZ'][cut])
        WBIBtaggerHAHM02.append(outBIBtaggerW['HAHM'][cut])
    print("\n 0-2 \n\n")
    print('#Events')
    print('Cuts02 = ', Cuts02)
    print('EventsFRVZ02 = ', EventsFRVZ)
    print('WEventsFRVZ02 = ', EventsWFRVZ)
    print('EventsHAHM02 = ', EventsHAHM)
    print('WEventsHAHM02 = ', EventsWHAHM)
    print('#DNN')
    print('CutsDNN02 = ', CutsDNN02)
    print('WdnnFRVZ02 = ',WdnnFRVZ02)
    print('WdnnHAHM02 = ', WdnnHAHM02)
    print('#DPhi')
    print('CutsDPhi02 = ', CutsDPhi02)
    print('WDPhiFRVZ02 = ', WDPhiFRVZ02)
    print('WDPhiHAHM02 = ', WDPhiHAHM02)
    print('#GapRatio')
    print('CutsGapRatio02 = ', CutsGapRatio02)
    print('WGapRatioFRVZ02 = ', WGapRatioFRVZ02)
    print('WGapRatioHAHM02 = ', WGapRatioHAHM02)
    print('#Timing')
    print('CutsTiming02 = ', CutsTiming02)
    print('WTimingFRVZ02 = ', WTimingFRVZ02)
    print('WTimingHAHM02 = ', WTimingHAHM02)
    print('#BIBtagger')
    print('CutsBIBtagger02 = ', CutsBIBtagger02)
    print('WBIBtaggerFRVZ02 = ', WBIBtaggerFRVZ02)
    print('WBIBtaggerHAHM02 = ', WBIBtaggerHAHM02)

def RunHistStudy22(outputFiles):
    #Conteo de eventos
    outEvents  = OrderedDict()
    outEventsW  = OrderedDict()
    outEventsMET = OrderedDict()
    outEventsDPhi = OrderedDict()
    outEventsGapRatio = OrderedDict()
    outEventsTiming= OrderedDict()
    outEventsBIBtagger = OrderedDict()
    outEventsJVTtagger = OrderedDict()
    for out in outputFiles.keys():
        processEvents = OrderedDict()
        processEventsW = OrderedDict()
        processEventsMET = OrderedDict()
        processEventsDPhi = OrderedDict()
        processEventsGapRatio = OrderedDict()
        processEventsTiming =  OrderedDict()
        processEventsBIBtagger = OrderedDict()
        processEventsJVTtagger = OrderedDict()
        f = ROOT.TFile.Open(outputFiles[out], "READ")
        #Total cuts
        Cuts22 = list()
        h22 =  f.CutEvents22
        h22W = f.CutWEvents22
        i = 0
        while i < h22.GetNbinsX():
            Cuts22.append(h22.GetXaxis().GetBinLabel(i+1))
            processEvents[h22.GetXaxis().GetBinLabel(i+1)] = int(h22.GetBinContent(i+1))
            processEventsW[h22W.GetXaxis().GetBinLabel(i+1)] = int(h22W.GetBinContent(i+1))
            i += 1
        outEvents[out] = processEvents
        outEventsW[out]  = processEventsW
        #MET
        hMET = f.histMET22
        i = 0
        while i < hMET.GetNbinsX():
            processEventsMET[hMET.GetXaxis().GetBinLabel(i+1)] = int(hMET.GetBinContent(i+1)) #{'0': 1337 events, '10': ...}
            i += 1
        outEventsMET[out] = processEventsMET
        #Dphi
        hDPhi = f.histDeltaPhi22
        i = 0
        while i < hDPhi.GetNbinsX():
            processEventsDPhi[hDPhi.GetXaxis().GetBinLabel(i+1)] = int(hDPhi.GetBinContent(i+1))
            i += 1
        outEventsDPhi[out] = processEventsDPhi
        #GaRatio
        hGapRatio= f.histGapRatio22
        i = 0
        while i < hGapRatio.GetNbinsX():
            processEventsGapRatio[hGapRatio.GetXaxis().GetBinLabel(i+1)] = int(hGapRatio.GetBinContent(i+1))
            i += 1
        outEventsGapRatio[out] = processEventsGapRatio
        #Timing
        hTiming= f.histTiming22
        i = 0
        while i < hTiming.GetNbinsX():
            processEventsTiming[hTiming.GetXaxis().GetBinLabel(i+1)] = int(hTiming.GetBinContent(i+1))
            i += 1
        outEventsTiming[out] = processEventsTiming
        #BIBtagger
        hBIBtagger = f.histBIBtagger22
        i = 0
        while i < hBIBtagger.GetNbinsX():
            processEventsBIBtagger[hBIBtagger.GetXaxis().GetBinLabel(i+1)] = int(hBIBtagger.GetBinContent(i+1))
            i += 1
        outEventsBIBtagger[out] = processEventsBIBtagger
        #JVT
        hJVTtagger = f.histJVTtagger22
        i = 0
        while i < hJVTtagger.GetNbinsX():
            processEventsJVTtagger[hJVTtagger.GetXaxis().GetBinLabel(i+1)] = int(hJVTtagger.GetBinContent(i+1))
            i += 1
        outEventsJVTtagger[out] = processEventsJVTtagger
    BKG = ['QCD' + str(n) for n in range(2, 12)]
    EventsFRVZ22 = list()
    WEventsFRVZ22 = list()
    EventsHAHM22 = list()
    WEventsHAHM22 = list()
    METcuts = [n for n in range(0, 310, 10)]
    METbkg = list()
    METeventsFRVZ = list()
    METeventsHAHM = list()
    METsigFRVZ = list()
    METsigHAHM = list()
    DPhiCuts = [n * 0.1 for n in range(31)]
    DPhiBkg = list()
    DPhiEventsFRVZ = list()
    DPhiEventsHAHM = list()
    DPhiSigFRVZ = list()
    DPhiSigHAHM = list()
    GapRatioCuts = [n * 0.01 for n in range(90, 103)]
    GapRatioBkg = list()
    GapRatioEventsFRVZ = list()
    GapRatioEventsHAHM = list()
    GapRatioSigFRVZ =  list()
    GapRatioSigHAHM =  list()
    TimingCuts = [n * 0.1 for n in range(0, 52, 4)]
    TimingBkg = list()
    TimingEventsFRVZ = list()
    TimingEventsHAHM = list()
    TimingSigFRVZ = list()
    TimingSigHAHM = list()
    BIBtaggerCuts= [n * 0.1 for n in range (11)]
    BIBtaggerBkg = list()
    BIBtaggerEventsFRVZ = list()
    BIBtaggerEventsHAHM = list()
    BIBtaggerFRVZ = list()
    BIBtaggerHAHM =list()
    JVTtaggerCuts = [n * 0.1 for n in range(11)]
    JVTtaggerBkg = list()
    JVTtaggerEventsFRVZ = list()
    JVTtaggerEventsHAHM = list()
    JVTtaggerFRVZ = list()
    JVTtaggerHAHM = list()
    
    for cut in Cuts22:
        EventsFRVZ22.append(outEvents['FRVZ'][str(cut)])
        WEventsFRVZ22.append(outEventsW['FRVZ'][str(cut)])
        EventsHAHM22.append(outEvents['HAHM'][str(cut)])
        WEventsHAHM22.append(outEventsW['HAHM'][str(cut)])
    for cut in METcuts:
        total_bkg = 0
        for bkg in BKG:
            total_bkg += outEventsMET[bkg][str(cut)]
        METbkg.append(total_bkg)
        METeventsFRVZ.append(outEventsMET['FRVZ'][str(cut)])
        METeventsHAHM.append(outEventsMET['HAHM'][str(cut)])
        METsigFRVZ.append(getSig(outEventsMET['FRVZ'][str(cut)], total_bkg))
        METsigHAHM.append(getSig(outEventsMET['HAHM'][str(cut)], total_bkg))
    for cut in DPhiCuts:
        total_bkg = 0
        for bkg in BKG:
            total_bkg += outEventsDPhi[bkg][str(cut)]
        DPhiBkg.append(total_bkg)
        DPhiEventsFRVZ.append(outEventsDPhi['FRVZ'][str(cut)])
        DPhiEventsHAHM.append(outEventsDPhi['HAHM'][str(cut)])
        DPhiSigFRVZ.append(getSig(outEventsDPhi['FRVZ'][str(cut)], total_bkg))
        DPhiSigHAHM.append(getSig(outEventsDPhi['HAHM'][str(cut)], total_bkg))
    for cut in GapRatioCuts:
        total_bkg = 0
        for bkg in BKG:
            total_bkg += outEventsGapRatio[bkg][str(cut)]
        GapRatioBkg.append(total_bkg)
        GapRatioEventsFRVZ.append(outEventsGapRatio['FRVZ'][str(cut)])
        GapRatioEventsHAHM.append(outEventsGapRatio['HAHM'][str(cut)])
        GapRatioSigFRVZ.append(getSig(outEventsGapRatio['FRVZ'][str(cut)], total_bkg))
        GapRatioSigHAHM.append(getSig(outEventsGapRatio['HAHM'][str(cut)], total_bkg))
    for cut in TimingCuts:
        total_bkg = 0    
        for bkg in BKG:
            total_bkg += outEventsTiming[bkg][str(cut)]
        TimingBkg.append(total_bkg)
        TimingEventsFRVZ.append(outEventsTiming['FRVZ'][str(cut)])
        TimingEventsHAHM.append(outEventsTiming['HAHM'][str(cut)])
        TimingSigFRVZ.append(getSig(outEventsTiming['FRVZ'][str(cut)], total_bkg))
        TimingSigHAHM.append(getSig(outEventsTiming['HAHM'][str(cut)], total_bkg))
    for cut in BIBtaggerCuts:
        total_bkg = 0
        for bkg in BKG:
            total_bkg += outEventsBIBtagger[bkg][str(cut)]
        BIBtaggerBkg.append(total_bkg)
        BIBtaggerEventsFRVZ.append(outEventsBIBtagger['FRVZ'][str(cut)])
        BIBtaggerEventsHAHM.append(outEventsBIBtagger['HAHM'][str(cut)])
        BIBtaggerFRVZ.append(getSig(outEventsBIBtagger['FRVZ'][str(cut)], total_bkg))
        BIBtaggerHAHM.append(getSig(outEventsBIBtagger['HAHM'][str(cut)], total_bkg))
    for cut in JVTtaggerCuts:
        total_bkg = 0
        for bkg in BKG:
            total_bkg += outEventsJVTtagger[bkg][str(cut)]
        JVTtaggerBkg.append(total_bkg)
        JVTtaggerEventsFRVZ.append(outEventsJVTtagger['FRVZ'][str(cut)])
        JVTtaggerEventsHAHM.append(outEventsJVTtagger['HAHM'][str(cut)])
        JVTtaggerFRVZ.append(getSig(outEventsJVTtagger['FRVZ'][str(cut)], total_bkg))
        JVTtaggerHAHM.append(getSig(outEventsJVTtagger['HAHM'][str(cut)], total_bkg))

    #print  significances
    print('\n 2-2 \n\n')
    print('#Events')
    print('Cuts22 = ', Cuts22)
    print('EventsFRVZ22 = ', EventsFRVZ22)
    print('WEventsFRVZ22 = ', WEventsFRVZ22)
    print('EventsHAHM22 = ', EventsHAHM22)
    print('WEventsHAHM22 = ', WEventsHAHM22)
    print('#MET')
    print('METbkg22 = ', METbkg)
    print('METeventsFRVZ22 = ', METeventsFRVZ)
    print('METeventsHAHM22 = ', METeventsHAHM)
    print('METCuts22 = ', METcuts)
    print('METFRVZ22 = ', METsigFRVZ)
    print('METHAHM22 = ', METsigHAHM)
    print('#DeltaPhi')
    print('WDPhiBkg22 = ', DPhiBkg)
    print('WDPhiFRVZ22 = ', DPhiEventsFRVZ)
    print('WDPhiHAHM22 = ', DPhiEventsHAHM)
    print('CutsDPhi22 = ', DPhiCuts)
    print('SigDPhiFRVZ22 = ', DPhiSigFRVZ)
    print('SigDPhiHAHM22 = ', DPhiSigHAHM)
    print('#GapRatio')
    print('WGapRatioBkg22 = ', GapRatioBkg)
    print('WGapRatioFRVZ22 = ', GapRatioEventsFRVZ)
    print('WGapRatioHAHM22 = ', GapRatioEventsHAHM)
    print('CutsGapRatio22 = ', GapRatioCuts)
    print('SigGapRatioFRVZ22 = ', GapRatioSigFRVZ)
    print('SigGapRatioHAHM22 = ', GapRatioSigHAHM)
    print('#Timing')
    print('WTimingBkg22 = ', TimingBkg)
    print('WTimingFRVZ22 = ', TimingEventsFRVZ)
    print('WTimingHAHM22 = ', TimingEventsHAHM)
    print('CutsTiming22 = ', TimingCuts)
    print('SigTimingFRVZ22 = ', TimingSigFRVZ)
    print('SigTimingHAHM22 = ', TimingSigHAHM)
    print('#BIBtagger')
    print('WBIBtaggerBkg22 = ', BIBtaggerBkg)
    print('WBIBtaggerFRVZ22 = ', BIBtaggerEventsFRVZ)
    print('WBIBtaggerHAHM22 = ', BIBtaggerEventsHAHM)
    print('CutsBIBtagger22 = ', BIBtaggerCuts)
    print('SigBIBtaggerFRVZ22 = ', BIBtaggerFRVZ)
    print('SigBIBtaggerHAHM22 = ', BIBtaggerHAHM)
    print('#JVT')
    print('WjvtBkg22 = ', JVTtaggerBkg)
    print('WjvtFRVZ22 = ', JVTtaggerEventsFRVZ)
    print('WjvtHAHM22 = ', JVTtaggerEventsHAHM)
    print('CutsJVT22 = ', JVTtaggerCuts)
    print('SigJVTFRVZ22 = ', JVTtaggerFRVZ)
    print('SigJVTHAHM22 = ', JVTtaggerHAHM)

def TruthStudy(outputFiles):

    TruthPtLabels = list()
    outTruthPT = OrderedDict()
    for out in outputFiles.keys():
        processTruthPT = OrderedDict()
        print('Processing Truth for {}'.format(out))
        f = c
        hPT = f.h_truthDP_pT
        i = 0
        while i < hPT.GetNbinsX():
            processTruthPT[hPT.GetXaxis().GetBinLabel(i+1)] = int(hPT.GetBinContent(i+1))
            TruthPtLabels.append(hPT.GetXaxis().GetBinLabel(i+1))
            i += 1
        outTruthPT[out] = processTruthPT
    
    EventsTruthPtFRVZ = list()
    EventsTruthPtHAHM = list()
    for label in TruthPtLabels:
        EventsTruthPtFRVZ.append(outTruthPT['FRVZ'][label])
        EventsTruthPtHAHM.append(outTruthPT['HAHM'][label])
    print('\n')
    print('#TruthStudy')
    print('TruthPtLabels = ', TruthPtLabels)
    print('TruthPtFRVZ = ', EventsTruthPtFRVZ)
    print('TruthPtHAHM = ', EventsTruthPtHAHM)

def TruthDraw(outputFiles):
    canvas = ROOT.TCanvas("canvas", "canvas")
    canvas.cd() 

    f_HAHM = ROOT.TFile.Open(outputFiles['HAHM'], "READ")
    hHAHMpT = f_HAHM.Get("h_truthDP_pT")
    hHAHMpT.SetDirectory(0)
    #hHAHMpT.SetFillcolor()
    hHAHMpT.Draw("h")
    hHAHMpT.SetLineColor(ROOT.kViolet)
    f_HAHM.Close()
    
    f_FRVZ = ROOT.TFile.Open(outputFiles['FRVZ'], "READ")
    hFRVZpT = f_FRVZ.Get("h_truthDP_pT")
    hFRVZpT.SetLineColor(ROOT.kGreen)
    hFRVZpT.SetDirectory(0)
    hFRVZpT.Draw("SAME")
    f_FRVZ.Close()

    myline= ROOT.TLine(20,0,20,990)
    myline.SetLineStyle(9)
    myline.Draw("same")


    
    canvas.Draw()
    canvas.SaveAs('Truth.png')



if __name__ == "__main__":
    inputFiles = {'FRVZ': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r9364.root"],
                'HAHM': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508929_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508929_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508929_r9364.root"],
                #'QCD2': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364702_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364702_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364702_r9364.root"],
                #'QCD3': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364703_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364703_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364703_r9364.root"],
                #'QCD4': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364704_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364704_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364704_r9364.root"],
                #'QCD5': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364705_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364705_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364705_r9364.root"],
                #'QCD6': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364706_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364706_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364706_r9364.root"],
                #'QCD7': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364707_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364707_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364707_r9364.root"],
                #'QCD8': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364708_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364708_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364708_r9364.root"],
                #'QCD9': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364709_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364709_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364709_r9364.root"],
                #'QCD10': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364710_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364710_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364710_r9364.root"],
                #'QCD11': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364711_r10201.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364711_r10724.root",
                #            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/364711_r9364.root"],
                'W+jets': None,
                'Z+jets': None,
                'ttbar': None,
                'Single Top Quark': None,
                'WW': None,
                'WZ': None,
                'ZZ': None}
    outputFiles = {'FRVZ': 'outFRVZ.root',
                    'HAHM': 'outHAHM.root',
                    'QCD2': 'outQCD2.root',
                    'QCD3': 'outQCD3.root',
                    'QCD4': 'outQCD4.root',
                    'QCD5': 'outQCD5.root',
                    'QCD6': 'outQCD6.root',
                    'QCD7': 'outQCD7.root',
                    'QCD8': 'outQCD8.root',
                    'QCD9': 'outQCD9.root',
                    'QCD10': 'outQCD10.root',
                    'QCD11': 'outQCD11.root',
                }
    
    #Test distintos ctau
    SampleInputs = {'HAHM1': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508929_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508929_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508929_r9364.root"], #ctau = 1.0 mm
                    'HAHM2': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508931_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508931_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508931_r9364.root"], #ctau = 25.0 mm
                    'HAHM3': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508932_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508932_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508932_r9364.root"], #ctau = 80.0 mm
                    'HAHM4': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/507580_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/507580_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/507580_r9364.root"], #ctau = 450.0 mm
                    'HAHM5': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/507581_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/507581_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/507581_r9364.root"], #ctau = 500 mm
                    'FRVZ1': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r9364.root"], #ctau = 2.0 mm
                    'FRVZ2': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508886_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508886_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508886_r9364.root"], #ctau = 7.0 mm
                    'FRVZ3': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508887_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508887_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508887_r9364.root"], #ctau =  15.0 mm
                    'FRVZ4': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508891_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508891_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508891_r9364.root"],#ctau = 600 mm
                    'FRVZ5': ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508893_r10201.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508893_r10724.root",
                            "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508893_r9364.root"]} #ctau = 1000 mm
    
    SampleOutputs = {'HAHM1':'SampleComparison/outHAHM1.root',
                    'HAHM2': 'SampleComparison/outHAHM2.root',
                    'HAHM3': 'SampleComparison/outHAHM3.root',
                    'HAHM4': 'SampleComparison/outHAHM4.root',
                    'HAHM5': 'SampleComparison/outHAHM5.root',
                    'FRVZ1': 'SampleComparison/outFRVZ1.root',
                    'FRVZ2': 'SampleComparison/outFRVZ2.root',
                    'FRVZ3': 'SampleComparison/outFRVZ3.root',
                    'FRVZ4': 'SampleComparison/outFRVZ4.root',
                    'FRVZ5': 'SampleComparison/outFRVZ5.root'}
    
    

#Cortes
RunFullAnalysis(inputFiles, outputFiles)

#Histogramas
#RunHistStudy00(outputFiles)
#RunHistStudy02(outputFiles)
#RunHistStudy22(outputFiles)

#Compare different ctau
#RunSampleComparison(SampleInputs, SampleOutputs)

#Truth
#TruthStudy(outputFiles)
#TruthDraw(outputFiles)

