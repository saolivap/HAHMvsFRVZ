#!/usr/bin/python
from __future__ import print_function
import argparse
import sys
import os
import ROOT
#from PrintCFresults import CFprinter
ROOT.gROOT.SetBatch(True)

print("\n\n\n")

### Mini Ntuple
def getOptions(args=sys.argv[1:]):
    ###  Argument parser ###
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("-i", "--input", 
                        help="Input files (either comma-separated .root files or .txt list of files)", 
                        type=str,
                        nargs="+")
    parser.add_argument("-o", "--output", 
                        help="Output file name (default is outfile) - .root is added if not present", 
                        default="outfile", 
                        type=str)
    parser.add_argument("-T","--fullNtuple", 
                        help="Run on T instead of miniT", 
                        action="store_true")
    parser.add_argument("--mode", 
                        help="to run specific cutflow",
                        default="Standard",
                        type=str)
    parser.add_argument("--doValidationTrees",
                        help="avoid producing validation trees (abcd and microT_XX)",
                        dest="doValidTrees",
                        default="0",
                        action="store_const",
                        const="1")
    parser.add_argument("--doLiRe",
                        help="compute curves for lifetime reweighting",
                        default="0",
                        action="store_const",
                        const="1")
    parser.add_argument("--doCalRatioSyst",
                        help="Use HLT emulated calRatio trigger",
                        default="no",
                        type=str)
    parser.add_argument("--truthHiggsPtReweighting",
                        help="apply higher order reweighting to the truth higgs pt",
                        default="no",
                        action="store_const",
                        const="yes"
                        )
    parser.add_argument("--tree",
                        default="miniT",
                        type=str)
                                                
    options = parser.parse_args(args)
    return options


if __name__ == "__main__":

    
    #inputFiles = ["/eos/user/i/ilongari/public/ntuples/displacedLJfullRun2/miniNtuple-01-06_DPJtagger4.1/311949_r9364.root",
    #              "/eos/user/i/ilongari/public/ntuples/displacedLJfullRun2/miniNtuple-01-06_DPJtagger4.1/311949_r10201.root",
    #
    #              "/eos/user/i/ilongari/public/ntuples/displacedLJfullRun2/miniNtuple-01-06_DPJtagger4.1/311949_r10724.root"]
    inputFiles = ["/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/508885_r9364.root"] #FRFV]
    ### Argument parser
    options = getOptions(sys.argv[1:])
    if options.input:
        inputFiles = []
        if type(options.input) is list:
            inputFiles = [ x for x in options.input if x.endswith(".root")]

        elif options.input.endswith(".txt"):
            lines = [line.rstrip('\n') for line in open(options.input)]
            for ntupleFile in lines:
                if ntupleFile.endswith(".root"):
                    inputFiles.append(ntupleFile)

        elif options.input.endswith(".root"):
            for ntupleFile in options.input.split(","):
                if ntupleFile.endswith(".root"):
                    inputFiles.append(options.input)


    outFileName = str(options.output)
    if not outFileName.endswith(".root"):
        outFileName = outFileName+".root"

    #### needed if you want to save clusters in another tree
    ROOT.gInterpreter.GenerateDictionary("vector<vector<float> >", "vector")
    ROOT.gInterpreter.GenerateDictionary("vector<vector<int> >", "vector")


    TChainName = options.tree
    if options.fullNtuple:
        TChainName="T"
        print(" > > > Running with full Ntuple")
    print("* * * Creating new TChain with tree {} * * * \n".format(TChainName))
    ch = ROOT.TChain(TChainName)

    print("\nAdding ntuples:")
    for inFile in inputFiles:
        ch.Add(inFile)
        print("   {}".format(inFile))

    nTotalEntries = ch.GetEntries()
    print("\n Total entries: ", nTotalEntries)


    print("\n\n * * * Processing LJexplorer * * *")

    arguments=[outFileName,
               str(nTotalEntries),
               options.mode,
               options.doValidTrees,
               options.doLiRe,
               options.doCalRatioSyst,
               options.truthHiggsPtReweighting]
    argumentsStr = " ".join(arguments)
    #print(argumentsStr)
    SC = ch.Process("LJcutAnalysis.cxx",argumentsStr)

    # Print results
    #if ( nTotalEntries > 0 and SC == 0 ):
    #    CFprinter(outFileName)

    exit(SC!=0)

