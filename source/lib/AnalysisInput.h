// Dear emacs, this is -*- c++ -*-
//#ifndef FULLEXAMPLE_MYLIBRARY_H
//#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"
#include "TChain.h"
#include "TSelector.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
#include "TTreeReaderArray.h"
#include "TStreamerInfo.h"


// Boost include(s):
// #include <boost/program_options.hpp>
#include <boost/progress.hpp>
//#include <boost/timer/progress_display.hpp>

// Text include(s):
#include <fstream>

// Vector include(s):
#include <vector>
#include <map>
#include <string>
#include "TH1D.h"
#include "TH2F.h"
#include <TMath.h>

#include<string_view>

//using namespace ROOT;

//#include "TTreeReader.h"
//#include "TTreeReaderArray.h"

class AnalysisInput {

public:
  

  AnalysisInput(Option option);
  ~AnalysisInput();
  void addBranches();
  void initialize();
  void finalize();
  void execute();
  float ctau(float partX, float partY, float partZ, float partE, float partM);
  float notZero(float numerator, float divisor);

  TTreeReader fReader;
  
  TTreeReaderValue<Bool_t> isMC = {fReader, "isMC"};
  TTreeReaderArray<float> etaLJ = {fReader, "etaLJ"};
  TTreeReaderArray<float> phiLJ = {fReader, "phiLJ"};
  TTreeReaderArray<float> ptLJ = {fReader, "ptLJ"};
  TTreeReaderArray<int> types = {fReader, "types"};
  TTreeReaderArray<int> LJmuon_index = {fReader, "LJmuon_index"};
  TTreeReaderArray<int> LJmuon_author = {fReader, "LJmuon_author"};
  TTreeReaderArray<int> LJmuon_type = {fReader, "LJmuon_type"};
  TTreeReaderArray<double> LJmuon_RPCtime = {fReader, "LJmuon_RPCtime"};
  TTreeReaderArray<float> LJmuon_eta = {fReader, "LJmuon_eta"};
  TTreeReaderArray<float> LJmuon_phi = {fReader, "LJmuon_phi"};
  TTreeReaderArray<float> LJmuon_pt = {fReader, "LJmuon_pt"};
  TTreeReaderValue<Int_t> lead_index = {fReader, "lead_index"};
  TTreeReaderValue<Int_t> far_index = {fReader, "far_index"};
  //int *lead_index = new int(-1);
  
  
private:

  // De Iacopo
  
  //TTreeReader myReader("ntuple", myFile);
  

  //

  Option option_;
  TFile *output;
  TTree *tree_out;
  TFile *input;
  TChain *chain_in;

   // File name
  TString fname;
  TString outname;

  // Tree files folder
  //TString filesFolder = "/home/czilleruelo/Samples/ggf/FRVZ/";
  TString filesFolder = "/home/saolivap/WorkArea/samples/miniNtuple/miniNtuple-01-06_DPJtagger4.1/";
  TString pathNtuples = "/home/czilleruelo/ggfcris/ntuples/";

  // Number of events
  int n_events;
  int n_zero;
  int accepted_events;

   // Event Counter
  int n_counters = 1;
  int count[13] = { 0 };
  float total_weight[13] = { 0 };

  //Tre input de Iacopo (solo los utilizados)

  //Bool_t isMC[10000];
  //Float_t etaLJ[10000];
  //Float_t phiLJ[10000];
  //Float_t ptLJ[10000];
  //Int_t types[10000];
  //Int_t LJmuon_index[10000];
  //Int_t LJmuon_author[10000];
  //Int_t LJmuon_type[10000];
  //double LJmuon_RPCtime[10000];
  //Float_t LJmuon_eta[10000];
  //Float_t LJmuon_phi[10000];
  //Float_t LJmuon_pt[10000];
  //Int_t lead_index[10000];
  //Int_t far_index[10000];

  
  



//Tree input variables
  //Float_t isMC;
  //Float_t RunNumber;
  //Float_t dsid;
  //Float_t eventNumber;
  //Float_t amiXsection;
  //Float_t filterEff;
  //Float_t kFactor;
  //Float_t sumWeightPRW;
  //Float_t trigQCD;
  //Float_t trigQCD_prescale;
  //Float_t trig;
  //Float_t hasPV;
  //Float_t lead_index;
  //Float_t far_index;
  //Float_t far15_index;
  //Float_t far20_index;
  //Float_t etaZ;
  //Float_t phiZ;
  //Float_t ptZ;
  //Float_t mZ;
  //Float_t eZ;
  //Float_t mT;
  //Float_t mT_isSignal;
//
  //Float_t muon_author;
  //Float_t muon_allAuthors;
  //Float_t muon_type;
  //Float_t muon_eta;
  //Float_t muon_phi;
  //Float_t muon_pt;
  //Float_t muon_e;
  //Float_t muon_charge;
  //Float_t muon_isZ;
  //Float_t muon_isW;
  //Float_t muon_isFromPV_baseline;
  //Float_t muon_isFromPV_signal;
  //Float_t muon_isIsolated;
  //Float_t muon_isIsoCustom;
  //Float_t muon_quality;
  //Float_t muon_isBaseline;
  //Float_t muon_isSignal;
  //Float_t muon_isOR;
//
  //Float_t ele_eta;
  //Float_t ele_phi;
  //Float_t ele_pt;
  //Float_t ele_e;
  //Float_t ele_charge;
  //Float_t ele_isZ;
  //Float_t ele_isW;
  //Float_t ele_isLooseBLLHElectron;
  //Float_t ele_isFromPV_baseline;
  //Float_t ele_isIsoCustom;
  //Float_t ele_isTightLHElectron;
  //Float_t ele_isFromPV_signal;
  //Float_t ele_isIsolated;
  //Float_t ele_isBaseline;
  //Float_t ele_isSignal;
  //Float_t ele_isOR;
//
  //Float_t etaLJ;
  //Float_t phiLJ;
  //Float_t ptLJ;
  //Float_t types;
  //Float_t isoID;
  //Float_t LJ_index;
  //Float_t LJoverlapLepton;
  //Float_t HT_20;
  //Float_t HT_20_30;
  //Float_t MET;
  //Float_t METsig;
  //Float_t METOSqrtHT;
  //Float_t MET_phi;
  //Float_t MET_truth;
  //Float_t MET_phi_truth;
  //Float_t weight;
  //Float_t puWeight;
  //Float_t mcWeight;
//
  //Float_t LJjet_index;
  //Float_t LJjet_eta;
  //Float_t LJjet_phi;
  //Float_t LJjet_pt;
  //Float_t LJjet_width;
  //Float_t LJjet_EMfrac;
  //Float_t LJjet_timing;
  //Float_t LJjet_jvt;
  //Float_t LJjet_gapRatio;
  //Float_t LJjet_IsBIB;
  //Float_t LJjet_m;
//
  //Float_t LJmuon_index;
  //Float_t LJmuon_match;
  //Float_t LJmuon_author;
  //Float_t LJmuon_allAuthors;
  //Float_t LJmuon_type;
  //Float_t LJmuon_z0;
  //Float_t LJmuon_d0;
  //Float_t LJmuon_RPCtime;
  //Float_t LJmuon_RPCinfo;
  //Float_t LJmuon_eta;
  //Float_t LJmuon_phi;
  //Float_t LJmuon_pt;
  //Float_t LJmuon_charge;
//
  //Float_t jet_cal_eta;
  //Float_t jet_cal_phi;
  //Float_t jet_cal_pt;
  //Float_t jet_cal_e;
  //Float_t jet_cal_width;
  //Float_t jet_cal_EMfrac;
  //Float_t jet_cal_timing;
  //Float_t jet_cal_jvt;
  //Float_t jet_cal_gapRatio;
  //Float_t jet_cal_IsBIB;
  //Float_t jet_cal_isLLP;
  //Float_t jet_cal_isSTD;
  //Float_t jet_cal_isSTDOR;
  //Float_t jet_cal_isW;
  //Float_t jet_cal_isZ;
  //Float_t jet_cal_isMuon;
  //Float_t jet_cal_isLJ;
  //Float_t jet_cal_isBjet;
  //Float_t jet_cal_MV2c10;
//
  //Float_t truthPdgId;
  //Float_t truthEta;
  //Float_t truthPhi;
  //Float_t truthPt;
  //Float_t truthE;
  //Float_t truthCharge;
  //Float_t truthBarcode;
  //Float_t truthProdVtx_x;
  //Float_t truthProdVtx_y;
  //Float_t truthProdVtx_z;
  //Float_t truthDecayVtx_x;
  //Float_t truthDecayVtx_y;
  //Float_t truthDecayVtx_z;
  //Float_t truthNchildren;
  //Float_t truthNparents;
  //Float_t truthDecayType;
//
  //Float_t childPdgld;
  //Float_t childEta;
  //Float_t childPhi;
  //Float_t childPt;
  //Float_t childE;
  //Float_t childCharge;
  //Float_t childBarcode;
  //Float_t childMomBarcode;
  //Float_t childProdVtx_x;
  //Float_t childProdVtx_y;
  //Float_t childProdVtx_z;
  //Float_t childDecayVtx_x;
  //Float_t childDecayVtx_y;
  //Float_t childDecayVtx_z;
  //Float_t childNchildren;
  //Float_t childNparents;
//
  //Float_t trigEmuCalRatio;
  //Float_t LJmuon_DNNscore;
  //Float_t LJjet_DPJtagger;
  //Float_t LJjet_BIBtagger;

  // List of terms for normalizing entrances
  //std::list<Float_t> listBranches = {
  //  isMC,
  //RunNumber,
  //dsid,
  //eventNumber,
  //amiXsection,
  //filterEff,
  //kFactor,
  //sumWeightPRW,
  //trigQCD,
  //trigQCD_prescale,
  //trig,
  //hasPV,
  //lead_index,
  //far_index,
  //far15_index,
  //far20_index,
  //etaZ,
  //phiZ,
  //ptZ,
  //mZ,
  //eZ,
  //mT,
  //mT_isSignal,
  //muon_author,
  //muon_allAuthors,
  //muon_type,
  //muon_eta,
  //muon_phi,
  //muon_pt,
  //muon_e,
  //muon_charge,
  //muon_isZ,
  //muon_isW,
  //muon_isFromPV_baseline,
  //muon_isFromPV_signal,
  //muon_isIsolated,
  //muon_isIsoCustom,
  //muon_quality,
  //muon_isBaseline,
  //muon_isSignal,
  //muon_isOR,
  //ele_eta,
  //ele_phi,
  //ele_pt,
  //ele_e,
  //ele_charge,
  //ele_isZ,
  //ele_isW,
  //ele_isLooseBLLHElectron,
  //ele_isFromPV_baseline,
  //ele_isIsoCustom,
  //ele_isTightLHElectron,
  //ele_isFromPV_signal,
  //ele_isIsolated,
  //ele_isBaseline,
  //ele_isSignal,
  //ele_isOR,
  //etaLJ,
  //phiLJ,
  //ptLJ,
  //types,
  //isoID,
  //LJ_index,
  //LJoverlapLepton,
  //HT_20,
  //HT_20_30,
  //MET,
  //METsig,
  //METOSqrtHT,
  //MET_phi,
  //MET_truth,
  //MET_phi_truth,
  //weight,
  //puWeight,
  //mcWeight,
  //LJjet_index,
  //LJjet_eta,
  //LJjet_phi,
  //LJjet_pt,
  //LJjet_width,
  //LJjet_EMfrac,
  //LJjet_timing,
  //LJjet_jvt,
  //LJjet_gapRatio,
  //LJjet_IsBIB,
  //LJjet_m,
  //LJmuon_index,
  //LJmuon_match,
  //LJmuon_author,
  //LJmuon_allAuthors,
  //LJmuon_type,
  //LJmuon_z0,
  //LJmuon_d0,
  //LJmuon_RPCtime,
  //LJmuon_RPCinfo,
  //LJmuon_eta,
  //LJmuon_phi,
  //LJmuon_pt,
  //LJmuon_charge,
  //jet_cal_eta,
  //jet_cal_phi,
  //jet_cal_pt,
  //jet_cal_e,
  //jet_cal_width,
  //jet_cal_EMfrac,
  //jet_cal_timing,
  //jet_cal_jvt,
  //jet_cal_gapRatio,
  //jet_cal_IsBIB,
  //jet_cal_isLLP,
  //jet_cal_isSTD,
  //jet_cal_isSTDOR,
  //jet_cal_isW,
  //jet_cal_isZ,
  //jet_cal_isMuon,
  //jet_cal_isLJ,
  //jet_cal_isBjet,
  //jet_cal_MV2c10,
  //truthPdgId,
  //truthEta,
  //truthPhi,
  //truthPt,
  //truthE,
  //truthCharge,
  //truthBarcode,
  //truthProdVtx_x,
  //truthProdVtx_y,
  //truthProdVtx_z,
  //truthDecayVtx_x,
  //truthDecayVtx_y,
  //truthDecayVtx_z,
  //truthNchildren,
  //truthNparents,
  //truthDecayType,
  //childPdgld,
  //childEta,
  //childPhi,
  //childPt,
  //childE,
  //childCharge,
  //childBarcode,
  //childMomBarcode,
  //childProdVtx_x,
  //childProdVtx_y,
  //childProdVtx_z,
  //childDecayVtx_x,
  //childDecayVtx_y,
  //childDecayVtx_z,
  //childNchildren,
  //childNparents,
  //trigEmuCalRatio,
  //LJmuon_DNNscore,
  //LJjet_DPJtagger,
  //LJjet_BIBtagger,
  //};

  //CSV files
  std::ofstream ArchivoCSV1;

};

//#endif