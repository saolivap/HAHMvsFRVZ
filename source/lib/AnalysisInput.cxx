// System include(s):
#include <iostream>
#include <iomanip>
#include <list>
#include <vector>
#include <algorithm>
#include <TMath.h>

// Local include(s):
#include "AnalysisInput.h"

AnalysisInput::AnalysisInput(Option option)
{

    std::cout << std::endl;
    std::cout << "---AnalysisInput ::  Analysis on"  <<  option;

    option_ = option;

    if ( option_ == opcion_1  ) std::cout << ": Test 1  -> todo de panini" << std::endl;
    
}

AnalysisInput::~AnalysisInput()
{

    delete input;
    delete chain_in;
    delete output;
    delete tree_out;
}

float AnalysisInput::notZero(float numerator, float denominator) {

  if ( denominator == 0) n_zero++;//std::cout << "Divided by zero. -checked div";
  else return numerator / denominator;
  
}

float AnalysisInput::ctau(float partX, float partY, float partZ, float partE, float partM) {
  
  float decayLength = TMath::Sqrt( pow(partX,2) + pow(partY,2) + pow(partZ,2) );
  float gamma = notZero(partE,partM);
  float beta = TMath::Sqrt( 1 - 1/pow(gamma, 2) );
  float beta_gamma = beta*gamma;
  float ctau = notZero(decayLength, beta_gamma);
  return ctau;
      
}

void AnalysisInput::initialize()
{

    //Tree integer variables (no vectors)
    n_events = 0;
    accepted_events = 0;

    //Test
    if (option_ == opcion_1) {
        std::cout<<"wenardo wenardooooooo"<<std::endl;
    }
}

void AnalysisInput::addBranches()
{
    // Load Input files
    chain_in = new TChain("miniT");
    if ( option_ == opcion_1) chain_in->Add(filesFolder + "508885_r9364.root");
    //if ( option_ == opcion_2) chain_in->Add(filesFolder + "311309_r10724.root");
    //if ( option_ == opcion_3) chain_in->Add(filesFolder + "311309_r9364.root");
    //if ( option_ == opcion_4) chain_in->Add(filesFolder + "311310_r10201.root");
    //if ( option_ == opcion_5) chain_in->Add(filesFolder + "311310_r10724.root");
    //if ( option_ == opcion_6) chain_in->Add(filesFolder + "311310_r9364.root");
    

    //chain_in->SetBranchAddress("isMC", &isMC);
    //chain_in->SetBranchAddress("RunNumber", &RunNumber);
    //chain_in->SetBranchAddress("dsid", &dsid);
    //chain_in->SetBranchAddress("eventNumber", &eventNumber);
    //chain_in->SetBranchAddress("amiXsection", &amiXsection);
    //chain_in->SetBranchAddress("filterEff", &filterEff);
    //chain_in->SetBranchAddress("kFactor", &kFactor);
    //chain_in->SetBranchAddress("sumWeightPRW", &sumWeightPRW);
    //chain_in->SetBranchAddress("trigQCD", &trigQCD);
    //chain_in->SetBranchAddress("trigQCD_prescale", &trigQCD_prescale);
    //chain_in->SetBranchAddress("trig", &trig);
    //chain_in->SetBranchAddress("hasPV", &hasPV);
    chain_in->SetBranchAddress("lead_index", &lead_index);
    chain_in->SetBranchAddress("far_index", &far_index);
    //chain_in->SetBranchAddress("far15_index", &far15_index);
    //chain_in->SetBranchAddress("far20_index", &far20_index);
    //chain_in->SetBranchAddress("etaZ", &etaZ);
    //chain_in->SetBranchAddress("phiZ", &phiZ);
    //chain_in->SetBranchAddress("ptZ", &ptZ);
    //chain_in->SetBranchAddress("mZ", &mZ);
    //chain_in->SetBranchAddress("eZ", &eZ);
    //chain_in->SetBranchAddress("mT",  &mT);
    //chain_in->SetBranchAddress("mT_isSignal", &mT_isSignal);
    //chain_in->SetBranchAddress("muon_author", &muon_author);
    //chain_in->SetBranchAddress("muon_allAuthors",&muon_allAuthors);
    //chain_in->SetBranchAddress("muon_type", &muon_type);
    //chain_in->SetBranchAddress("muon_eta", &muon_eta);
    //chain_in->SetBranchAddress("muon_phi", &muon_phi);
    //chain_in->SetBranchAddress("muon_pt", &muon_pt);
    //chain_in->SetBranchAddress("muon_e",&muon_e);
    //chain_in->SetBranchAddress("muon_charge", &muon_charge);
    //chain_in->SetBranchAddress("muon_isZ", &muon_isZ);
    //chain_in->SetBranchAddress("muon_isW",  &muon_isW);
    //chain_in->SetBranchAddress("muon_isFromPV_baseline",  &muon_isFromPV_baseline);
    //chain_in->SetBranchAddress("muon_isFromPV_signal",  &muon_isFromPV_signal);
    //chain_in->SetBranchAddress("muon_isIsolated", &muon_isIsolated);
    //chain_in->SetBranchAddress("muon_isIsoCustom", &muon_isIsoCustom);
    //chain_in->SetBranchAddress("muon_quality", &muon_quality);
    //chain_in->SetBranchAddress("muon_isBaseline", &muon_isBaseline);
    //chain_in->SetBranchAddress("muon_isSignal", &muon_isSignal);
    //chain_in->SetBranchAddress("muon_isOR", &muon_isOR);
    //chain_in->SetBranchAddress("ele_eta", &ele_eta);
    //chain_in->SetBranchAddress("ele_phi", &ele_phi);
    //chain_in->SetBranchAddress("ele_pt", &ele_pt);
    //chain_in->SetBranchAddress("ele_e", &ele_e);
    //chain_in->SetBranchAddress("ele_charge", &ele_charge);
    //chain_in->SetBranchAddress("ele_isZ", &ele_isZ);
    //chain_in->SetBranchAddress("ele_isW", &ele_isW);
    //chain_in->SetBranchAddress("ele_isLooseBLLHElectron", &ele_isLooseBLLHElectron);
    //chain_in->SetBranchAddress("ele_isFromPV_baseline", &ele_isFromPV_baseline);
    //chain_in->SetBranchAddress("ele_isIsoCustom", &ele_isIsoCustom);
    //chain_in->SetBranchAddress("ele_isTightLHElectron", &ele_isTightLHElectron);
    //chain_in->SetBranchAddress("ele_isFromPV_signal", &ele_isFromPV_signal);
    //chain_in->SetBranchAddress("ele_isIsolated", &ele_isIsolated);
    //chain_in->SetBranchAddress("ele_isBaseline", &ele_isBaseline);
    //chain_in->SetBranchAddress("ele_isSignal", &ele_isSignal);
    //chain_in->SetBranchAddress("ele_isOR", &ele_isOR);
    //chain_in->SetBranchAddress("etaLJ", &etaLJ);
    //chain_in->SetBranchAddress("phiLJ", &phiLJ);
    //chain_in->SetBranchAddress("ptLJ", &ptLJ);
    //chain_in->SetBranchAddress("types", &types);
    //chain_in->SetBranchAddress("isoID", &isoID);
    //chain_in->SetBranchAddress("LJ_index", &LJ_index);
    //chain_in->SetBranchAddress("LJoverlapLepton", &LJoverlapLepton);
    //chain_in->SetBranchAddress("HT_20", &HT_20);
    //chain_in->SetBranchAddress("HT_20_30", &HT_20_30);
    //chain_in->SetBranchAddress("MET", &MET);
    //chain_in->SetBranchAddress("METsig", &METsig);
    //chain_in->SetBranchAddress("METOSqrtHT", &METOSqrtHT);
    //chain_in->SetBranchAddress("MET_phi", &MET_phi);
    //chain_in->SetBranchAddress("MET_truth", &MET_truth);
    //chain_in->SetBranchAddress("MET_phi_truth", &MET_phi_truth);
    //chain_in->SetBranchAddress("weight", &weight);
    //chain_in->SetBranchAddress("puWeight", &puWeight);
    //chain_in->SetBranchAddress("mcWeight", &mcWeight);
    //chain_in->SetBranchAddress("LJjet_index", &LJjet_index);
    //chain_in->SetBranchAddress("LJjet_eta",  &LJjet_eta);
    //chain_in->SetBranchAddress("LJjet_phi", &LJjet_phi);
    //chain_in->SetBranchAddress("LJjet_pt",  &LJjet_pt);
    //chain_in->SetBranchAddress("LJjet_width", &LJjet_width);
    //chain_in->SetBranchAddress("LJjet_EMfrac", &LJjet_EMfrac);
    //chain_in->SetBranchAddress("LJjet_timing", &LJjet_timing);
    //chain_in->SetBranchAddress("LJjet_jvt", &LJjet_jvt);
    //chain_in->SetBranchAddress("LJjet_gapRatio", &LJjet_gapRatio);
    //chain_in->SetBranchAddress("LJjet_IsBIB", &LJjet_IsBIB);
    //chain_in->SetBranchAddress("LJjet_m", &LJjet_m);
    chain_in->SetBranchAddress("LJmuon_index", &LJmuon_index);
    //chain_in->SetBranchAddress("LJmuon_match", &LJmuon_match);
    chain_in->SetBranchAddress("LJmuon_author", &LJmuon_author);
    //chain_in->SetBranchAddress("LJmuon_allAuthors", &LJmuon_allAuthors);
    chain_in->SetBranchAddress("LJmuon_type", &LJmuon_type);
    //chain_in->SetBranchAddress("LJmuon_z0", &LJmuon_z0);
    //chain_in->SetBranchAddress("LJmuon_d0", &LJmuon_d0);
    chain_in->SetBranchAddress("LJmuon_RPCtime", &LJmuon_RPCtime);
    //chain_in->SetBranchAddress("LJmuon_RPCinfo", &LJmuon_RPCinfo);
    chain_in->SetBranchAddress("LJmuon_eta", &LJmuon_eta);
    chain_in->SetBranchAddress("LJmuon_phi", &LJmuon_phi);
    chain_in->SetBranchAddress("LJmuon_pt", &LJmuon_pt);
    //chain_in->SetBranchAddress("LJmuon_charge", &LJmuon_charge);
    //chain_in->SetBranchAddress("jet_cal_eta", &jet_cal_eta);
    //chain_in->SetBranchAddress("jet_cal_phi", &jet_cal_phi);
    //chain_in->SetBranchAddress("jet_cal_pt", &jet_cal_pt);
    //chain_in->SetBranchAddress("jet_cal_e", &jet_cal_e);
    //chain_in->SetBranchAddress("jet_cal_width",  &jet_cal_width);
    //chain_in->SetBranchAddress("jet_cal_EMfrac", &jet_cal_EMfrac);
    //chain_in->SetBranchAddress("jet_cal_timing", &jet_cal_timing);
    //chain_in->SetBranchAddress("jet_cal_jvt", &jet_cal_jvt);
    //chain_in->SetBranchAddress("jet_cal_gapRatio", &jet_cal_gapRatio);
    //chain_in->SetBranchAddress("jet_cal_IsBIB", &jet_cal_IsBIB);
    //chain_in->SetBranchAddress("jet_cal_isLLp", &jet_cal_isLLP);
    //chain_in->SetBranchAddress("jet_cal_isSTD", &jet_cal_isSTD);
    //chain_in->SetBranchAddress("jet_cal_isSTDOR", &jet_cal_isSTDOR);
    //chain_in->SetBranchAddress("jet_cal_isW", &jet_cal_isW);
    //chain_in->SetBranchAddress("jet_cal_isZ", &jet_cal_isZ);
    //chain_in->SetBranchAddress("jet_cal_isMuon", &jet_cal_isMuon);
    //chain_in->SetBranchAddress("jet_cal_isLJ", &jet_cal_isLJ);
    //chain_in->SetBranchAddress("jet_cal_isBjet", &jet_cal_isBjet);
    //chain_in->SetBranchAddress("jet_cal_MV2c10", &jet_cal_MV2c10);
    //chain_in->SetBranchAddress("truthPdgld", &truthPdgld);
    //chain_in->SetBranchAddress("truthEta", &truthEta);
    //chain_in->SetBranchAddress("truthPhi", &truthPhi);
    //chain_in->SetBranchAddress("truthPt", &truthPt);
    //chain_in->SetBranchAddress("truthE", &truthE);
    //chain_in->SetBranchAddress("truthChagre", &truthCharge);
    //chain_in->SetBranchAddress("truthBarcode", &truthBarcode);
    //chain_in->SetBranchAddress("truthProdVtx_x", &truthProdVtx_x);
    //chain_in->SetBranchAddress("truthProdVtx_y", &truthProdVtx_y);
    //chain_in->SetBranchAddress("truthProdVtx_z", &truthProdVtx_z);
    //chain_in->SetBranchAddress("truthDecayVtx_x", &truthDecayVtx_x);
    //chain_in->SetBranchAddress("truthDecayVtx_y", &truthDecayVtx_y);
    //chain_in->SetBranchAddress("truthDecayVtx_z", &truthDecayVtx_z);
    //chain_in->SetBranchAddress("truthNchildren", &truthNchildren);
    //chain_in->SetBranchAddress("truthNparents", &truthNparents);
    //chain_in->SetBranchAddress("truthDecayType", &truthDecayType);
    //chain_in->SetBranchAddress("childPdgld", &childPdgld);
    //chain_in->SetBranchAddress("childEta", &childEta);
    //chain_in->SetBranchAddress("childPhi",&childPhi);
    //chain_in->SetBranchAddress("childPt", &childPt);
    //chain_in->SetBranchAddress("childE", &childE);
    //chain_in->SetBranchAddress("childCharge", &childCharge);
    //chain_in->SetBranchAddress("childBarcode", &childBarcode);
    //chain_in->SetBranchAddress("childMomBarcode", &childMomBarcode);
    //chain_in->SetBranchAddress("childProdVtx_x", &childProdVtx_x);
    //chain_in->SetBranchAddress("childProdVtx_y", &childProdVtx_y);
    //chain_in->SetBranchAddress("childProdVtx_z", &childProdVtx_z);
    //chain_in->SetBranchAddress("childDecayProdVtx_x", &childDecayVtx_x);
    //chain_in->SetBranchAddress("childDecayProdVtx_y", &childDecayVtx_y);
    //chain_in->SetBranchAddress("childDecayProdVtx_z", &childDecayVtx_z);
    //chain_in->SetBranchAddress("childNchildren", &childNchildren);
    //chain_in->SetBranchAddress("childNparents", &childNparents);
    //chain_in->SetBranchAddress("trigEmuCalRatio", &trigEmuCalRatio);
    //chain_in->SetBranchAddress("LJmuon_DNNscore", &LJmuon_DNNscore);
    //chain_in->SetBranchAddress("LJjet_DPJtagger", &LJjet_DPJtagger);
    //chain_in->SetBranchAddress("LJjet_BIBtagger", &LJjet_BIBtagger);


    // Output files

    if ( option_ == opcion_1 ) outname = pathNtuples + "ggf_test.root";

    output = new TFile(outname, "RECREATE");
    tree_out= new TTree("Selection", "Selection");

    tree_out->Branch("isMC", &isMC);
    //tree_out->Branch("RunNumber", &RunNumber);
    //tree_out->Branch("dsid", &dsid);
    //tree_out->Branch("eventNumber", &eventNumber);
    //tree_out->Branch("amiXsection", &amiXsection);
    //tree_out->Branch("filterEff", &filterEff);
    //tree_out->Branch("kFactor", &kFactor);
    //tree_out->Branch("sumWeightPRW", &sumWeightPRW);
    //tree_out->Branch("trigQCD", &trigQCD);
    //tree_out->Branch("trigQCD_prescale", &trigQCD_prescale);
    //tree_out->Branch("trig", &trig);
    //tree_out->Branch("hasPV", &hasPV);
    //tree_out->Branch("lead_index", &lead_index);
    //tree_out->Branch("far15_index", &far15_index);
    //tree_out->Branch("far20_index", &far20_index);
    //tree_out->Branch("etaZ", &etaZ);
    //tree_out->Branch("phiZ", &phiZ);
    //tree_out->Branch("ptZ", &ptZ);
    //tree_out->Branch("mZ", &mZ);
    //tree_out->Branch("eZ", &eZ);
    //tree_out->Branch("mT",  &mT);
    //tree_out->Branch("mT_isSignal", &mT_isSignal);
    //tree_out->Branch("muon_author", &muon_author);
    //tree_out->Branch("muon_allAuthors",&muon_allAuthors);
    //tree_out->Branch("muon_type", &muon_type);
    //tree_out->Branch("muon_eta", &muon_eta);
    //tree_out->Branch("muon_phi", &muon_phi);
    //tree_out->Branch("muon_pt", &muon_pt);
    //tree_out->Branch("muon_e",&muon_e);
    //tree_out->Branch("muon_charge", &muon_charge);
    //tree_out->Branch("muon_isZ", &muon_isZ);
    //tree_out->Branch("muon_isW",  &muon_isW);
    //tree_out->Branch("muon_isFromPV_baseline",  &muon_isFromPV_baseline);
    //tree_out->Branch("muon_isFromPV_signal",  &muon_isFromPV_signal);
    //tree_out->Branch("muon_isIsolated", &muon_isIsolated);
    //tree_out->Branch("muon_isIsoCustom", &muon_isIsoCustom);
    //tree_out->Branch("muon_quality", &muon_quality);
    //tree_out->Branch("muon_isBaseline", &muon_isBaseline);
    //tree_out->Branch("muon_isSignal", &muon_isSignal);
    //tree_out->Branch("muon_isOR", &muon_isOR);
    //tree_out->Branch("ele_eta", &ele_eta);
    //tree_out->Branch("ele_phi", &ele_phi);
    //tree_out->Branch("ele_pt", &ele_pt);
    //tree_out->Branch("ele_e", &ele_e);
    //tree_out->Branch("ele_charge", &ele_charge);
    //tree_out->Branch("ele_isZ", &ele_isZ);
    //tree_out->Branch("ele_isW", &ele_isW);
    //tree_out->Branch("ele_isLooseBLLHElectron", &ele_isLooseBLLHElectron);
    //tree_out->Branch("ele_isFromPV_baseline", &ele_isFromPV_baseline);
    //tree_out->Branch("ele_isIsoCustom", &ele_isIsoCustom);
    //tree_out->Branch("ele_isTightLHElectron", &ele_isTightLHElectron);
    //tree_out->Branch("ele_isFromPV_signal", &ele_isFromPV_signal);
    //tree_out->Branch("ele_isIsolated", &ele_isIsolated);
    //tree_out->Branch("ele_isBaseline", &ele_isBaseline);
    //tree_out->Branch("ele_isSignal", &ele_isSignal);
    //tree_out->Branch("ele_isOR", &ele_isOR);
    tree_out->Branch("etaLJ", &etaLJ);
    tree_out->Branch("phiLJ", &phiLJ);
    tree_out->Branch("ptLJ", &ptLJ);
    tree_out->Branch("types", &types);
    //tree_out->Branch("isoID", &isoID);
    //tree_out->Branch("LJ_index", &LJ_index);
    //tree_out->Branch("LJoverlapLepton", &LJoverlapLepton);
    //tree_out->Branch("HT_20", &HT_20);
    //tree_out->Branch("HT_20_30", &HT_20_30);
    //tree_out->Branch("MET", &MET);
    //tree_out->Branch("METsig", &METsig);
    //tree_out->Branch("METOSqrtHT", &METOSqrtHT);
    //tree_out->Branch("MET_phi", &MET_phi);
    //tree_out->Branch("MET_truth", &MET_truth);
    //tree_out->Branch("MET_phi_truth", &MET_phi_truth);
    //tree_out->Branch("weight", &weight);
    //tree_out->Branch("puWeight", &puWeight);
    //tree_out->Branch("mcWeight", &mcWeight);
    //tree_out->Branch("LJjet_index", &LJjet_index);
    //tree_out->Branch("LJjet_eta",  &LJjet_eta);
    //tree_out->Branch("LJjet_phi", &LJjet_phi);
    //tree_out->Branch("LJjet_pt",  &LJjet_pt);
    //tree_out->Branch("LJjet_width", &LJjet_width);
    //tree_out->Branch("LJjet_EMfrac", &LJjet_EMfrac);
    //tree_out->Branch("LJjet_timing", &LJjet_timing);
    //tree_out->Branch("LJjet_jvt", &LJjet_jvt);
    //tree_out->Branch("LJjet_gapRatio", &LJjet_gapRatio);
    //tree_out->Branch("LJjet_isBIB", &LJjet_IsBIB);
    //tree_out->Branch("LJjet_m", &LJjet_m);
    tree_out->Branch("LJmuon_index", &LJmuon_index);
    //tree_out->Branch("LJmuon_match", &LJmuon_match);
    tree_out->Branch("LJmuon_author", &LJmuon_author);
    //tree_out->Branch("LJmuon_allAuthors", &LJmuon_allAuthors);
    tree_out->Branch("LJmuon_type", &LJmuon_type);
    //tree_out->Branch("LJmuon_z0", &LJmuon_z0);
    //tree_out->Branch("LJmuon_d0", &LJmuon_d0);
    tree_out->Branch("LJmuon_RPCtime", &LJmuon_RPCtime);
    //tree_out->Branch("LJmuon_RPCinfo", &LJmuon_RPCinfo);
    tree_out->Branch("LJmuon_eta", &LJmuon_eta);
    tree_out->Branch("LJmuon_phi", &LJmuon_phi);
    tree_out->Branch("LJmuon_pt", &LJmuon_pt);
    //tree_out->Branch("LJmuon_charge", &LJmuon_charge);
    //tree_out->Branch("jet_cal_eta", &jet_cal_eta);
    //tree_out->Branch("jet_cal_phi", &jet_cal_phi);
    //tree_out->Branch("jet_cal_pt", &jet_cal_pt);
    //tree_out->Branch("jet_cal_e", &jet_cal_e);
    //tree_out->Branch("jet_cal_width",  &jet_cal_width);
    //tree_out->Branch("jet_cal_EMfrac", &jet_cal_EMfrac);
    //tree_out->Branch("jet_cal_timing", &jet_cal_timing);
    //tree_out->Branch("jet_cal_jvt", &jet_cal_jvt);
    //tree_out->Branch("jet_cal_gapRatio", &jet_cal_gapRatio);
    //tree_out->Branch("jet_cal_IsBIB", &jet_cal_IsBIB);
    //tree_out->Branch("jet_cal_isLLp", &jet_cal_isLLP);
    //tree_out->Branch("jet_cal_isSTD", &jet_cal_isSTD);
    //tree_out->Branch("jet_cal_isSTDOR", &jet_cal_isSTDOR);
    //tree_out->Branch("jet_cal_isW", &jet_cal_isW);
    //tree_out->Branch("jet_cal_isZ", &jet_cal_isZ);
    //tree_out->Branch("jet_cal_isMuon", &jet_cal_isMuon);
    //tree_out->Branch("jet_cal_isLJ", &jet_cal_isLJ);
    //tree_out->Branch("jet_cal_isBjet", &jet_cal_isBjet);
    //tree_out->Branch("jet_cal_MV2c10", &jet_cal_MV2c10);
    //tree_out->Branch("truthPdgld", &truthPdgld);
    //tree_out->Branch("truthEta", &truthEta);
    //tree_out->Branch("truthPhi", &truthPhi);
    //tree_out->Branch("truthPt", &truthPt);
    //tree_out->Branch("truthE", &truthE);
    //tree_out->Branch("truthChagre", &truthCharge);
    //tree_out->Branch("truthBarcode", &truthBarcode);
    //tree_out->Branch("truthProdVtx_x", &truthProdVtx_x);
    //tree_out->Branch("truthProdVtx_y", &truthProdVtx_y);
    //tree_out->Branch("truthProdVtx_z", &truthProdVtx_z);
    //tree_out->Branch("truthDecayVtx_x", &truthDecayVtx_x);
    //tree_out->Branch("truthDecayVtx_y", &truthDecayVtx_y);
    //tree_out->Branch("truthDecayVtx_z", &truthDecayVtx_z);
    //tree_out->Branch("truthNchildren", &truthNchildren);
    //tree_out->Branch("truthNparents", &truthNparents);
    //tree_out->Branch("truthDecayType", &truthDecayType);
    //tree_out->Branch("childPdgld", &childPdgld);
    //tree_out->Branch("childEta", &childEta);
    //tree_out->Branch("childPhi",&childPhi);
    //tree_out->Branch("childPt", &childPt);
    //tree_out->Branch("childE", &childE);
    //tree_out->Branch("childCharge", &childCharge);
    //tree_out->Branch("childBarcode", &childBarcode);
    //tree_out->Branch("childMomBarcode", &childMomBarcode);
    //tree_out->Branch("childProdVtx_x", &childProdVtx_x);
    //tree_out->Branch("childProdVtx_y", &childProdVtx_y);
    //tree_out->Branch("childProdVtx_z", &childProdVtx_z);
    //tree_out->Branch("childDecayProdVtx_x", &childDecayVtx_x);
    //tree_out->Branch("childDecayProdVtx_y", &childDecayVtx_y);
    //tree_out->Branch("childDecayProdVtx_z", &childDecayVtx_z);
    //tree_out->Branch("childNchildren", &childNchildren);
    //tree_out->Branch("childNparents", &childNparents);
    //tree_out->Branch("trigEmuCalRatio", &trigEmuCalRatio);
    //tree_out->Branch("LJmuon_DNNscore", &LJmuon_DNNscore);
    //tree_out->Branch("LJjet_DPJtagger", &LJjet_DPJtagger);
    //tree_out->Branch("LJjet_BIBtagger", &LJjet_BIBtagger);

    std::cout << "fin de Add branchees" << std::endl;

}
void AnalysisInput::execute()
{
    //TTreeReader
    //TTreeReaderValue<Bool_t> isMC = {fReader, "isMC"};
    //TTreeReaderArray<Float_t> etaLJ = {fReader, "etaLJ"};
    //TTreeReaderArray<Float_t> phiLJ = {fReader, "phiLJ"};
    //TTreeReaderArray<Float_t> ptLJ = {fReader, "ptLJ"};
    //TTreeReaderArray<Int_t> types = {fReader, "types"};
    //TTreeReaderArray<Int_t> LJmuon_index = {fReader, "LJmuon_index"};
    //TTreeReaderArray<Int_t> LJmuon_author = {fReader, "LJmuon_author"};
    //TTreeReaderArray<Int_t> LJmuon_type = {fReader, "LJmuon_type"};
    //TTreeReaderArray<double> LJmuon_RPCtime = {fReader, "LJmuon_RPCtime"};
    //TTreeReaderArray<Float_t> LJmuon_eta = {fReader, "LJmuon_eta"};
    //TTreeReaderArray<Float_t> LJmuon_phi = {fReader, "LJmuon_phi"};
    //TTreeReaderArray<Float_t> LJmuon_pt = {fReader, "LJmuon_pt"};
    //TTreeReaderValue<Int_t> lead_index = {fReader, "lead_index"};
    //TTreeReaderValue<Int_t> far_index = {fReader, "far_index"};

    //Boost - progress bar
    n_events = chain_in->GetEntries();
    std::cout << "Processing: " << n_events << " events" << std::endl;
    boost::progress_display show_progress( n_events);

    //Cortes
    for (Int_t ievt=0; ievt<n_events; ievt++) {

        //Read branches per event
        chain_in->GetEntry(ievt);
        ++show_progress;
        
        
        
        //cortes LJ
         for ( unsigned int m = 0; m < LJmuon_eta.GetSize() ; ++m ) {
            // Muon eta cut
            if ( ( types[*lead_index] != 2 ) &&
	         ( LJmuon_index[m] == *lead_index ) && 
	         ( TMath::Abs( LJmuon_eta[m] ) > 1. && TMath::Abs( LJmuon_eta[m] ) < 1.1 ) )   continue;

            if ( ( types[*far_index] != 2 ) &&
	         ( LJmuon_index[m] == *far_index ) && 
	         ( TMath::Abs( LJmuon_eta[m] ) > 1. && TMath::Abs( LJmuon_eta[m] ) < 1.1 ) )   continue;

         }
        //Se llenan los trees de salida
        tree_out->Fill();
    }

    std::cout<< "---End of Event Loop---" << std::endl;
}

void AnalysisInput::finalize()
{
//    //Contamos los eventos después de los cortes
    for (int icount=0; icount<n_counters; icount++) {
//
        TString NameMC = "Events after ";
        if (icount==0) NameMC += "Muon eta"; 

        std::cout<< NameMC + ":" << count[icount] << std::endl;

    }
  // Write and close output file
    output->Write();
    output->Close();
    std::cout<<"parece que nada fallo"<<std::endl;
}


    

