//System inlude(s)
#include <iostream>

//Local include(s)
#include "AnalysisInput.h"

//ROOT include(s)
#include "TTree.h"

int Run( Option opt ) {

    AnalysisInput *DF = new  AnalysisInput(opt);
    DF->initialize();
    DF->addBranches();
    DF->execute();
    DF->finalize();

    //return gracefully
    return 0;
}

int main( int argc, char* argv[] ) {
    Run(opcion_1);

    //return gracefully
    return 0;

}

