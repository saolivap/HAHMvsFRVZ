// dear Emacs, this is -*- c++ -*-
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr 27 16:36:51 2020 by ROOT version 6.14/08
// from TTree miniT/T
// found on file: /eos/user/i/ilongari/public/ntuples/displacedLJfullRun2/miniNtuple/311949_10726.root
//////////////////////////////////////////////////////////

#ifndef LJexplorer_h
#define LJexplorer_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include "DSIDtool.h"

// Headers needed by this particular selector
#include <vector>
#include <map>
#include <string>
#include "TH1D.h"
#include "TH2F.h"
#include <TMath.h>

using namespace std;

class LJexplorer : public TSelector {
public :
  TTreeReader     fReader;  //!the tree reader
  TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain


  // Readers to access the data
  TTreeReaderValue<Bool_t> isMC = {fReader, "isMC"};
  TTreeReaderValue<Int_t> eventNumber = {fReader, "eventNumber"};
  //int *lead_index = new int(-1);
  //int *far_index = new int(-1);
  TTreeReaderValue<Int_t> lead_index = {fReader, "lead_index"};
  TTreeReaderValue<Int_t> far_index = {fReader, "far_index"};
  TTreeReaderArray<int> types = {fReader, "types"};
  TTreeReaderArray<int> LJmuon_index = {fReader, "LJmuon_index"};
  TTreeReaderArray<int> LJmuon_author = {fReader, "LJmuon_author"};
  TTreeReaderArray<int> LJmuon_type = {fReader, "LJmuon_type"};
  TTreeReaderArray<float> LJmuon_z0 = {fReader, "LJmuon_z0"};
  TTreeReaderArray<float> LJmuon_d0 = {fReader, "LJmuon_d0"};
  TTreeReaderArray<double> LJmuon_RPCtime = {fReader, "LJmuon_RPCtime"};
  TTreeReaderArray<float> LJmuon_eta = {fReader, "LJmuon_eta"};
  TTreeReaderArray<float> LJmuon_phi = {fReader, "LJmuon_phi"};
  TTreeReaderArray<float> LJmuon_pt = {fReader, "LJmuon_pt"};
  TTreeReaderArray<float> etaLJ = {fReader, "etaLJ"};
  TTreeReaderArray<float> phiLJ = {fReader, "phiLJ"};
  TTreeReaderArray<float> ptLJ = {fReader, "ptLJ"};
  TTreeReaderArray<int> LJjet_index = {fReader, "LJjet_index"};
  TTreeReaderArray<float> LJjet_EMfrac = {fReader, "LJjet_EMfrac"};
  TTreeReaderArray<float> LJjet_timing = {fReader, "LJjet_timing"};
  TTreeReaderArray<float> LJjet_gapRatio = {fReader, "LJjet_gapRatio"};
  TTreeReaderArray<float> LJjet_IsBIB = {fReader, "LJjet_IsBIB"};
  TTreeReaderArray<float> LJjet_jvt = {fReader, "LJjet_jvt"};
  TTreeReaderArray<float> LJjet_pt = {fReader, "LJjet_pt"};
  
  TTreeReaderArray<double> type0DNN_out = {fReader, "LJmuon_DNNscore"};
  TTreeReaderArray<double> type2CNN_out = {fReader, "LJjet_DPJtagger"};
  TTreeReaderArray<double>  LJjet_BIBtagger = {fReader, "LJjet_BIBtagger"};


  //Trigger
  TTreeReaderValue<Int_t> RunNumber = {fReader, "RunNumber"};
  TTreeReaderArray<string> trig_name = {fReader, "trig_name"};
  TTreeReaderArray<int> trig = {fReader, "trig"};
  //QCD
  TTreeReaderArray<int> trigQCD = {fReader, "trigQCD"};
  TTreeReaderArray<string> trigQCD_name = {fReader, "trigQCD_name"};
  TTreeReaderArray<float> trigQCD_prescale = {fReader, "trigQCD_prescale"};
  

  //PV
  TTreeReaderValue<Bool_t> hasPV = {fReader, "hasPV"};

  //VH Orthogonality
  TTreeReaderArray<bool> muon_isSignal = {fReader, "muon_isSignal"};
  TTreeReaderArray<float> muon_pt = {fReader, "muon_pt"};
  TTreeReaderArray<float> ele_pt = {fReader, "ele_pt"};
  TTreeReaderArray<bool> ele_isSignal = {fReader, "ele_isSignal"};
  //VBF Orthogonality
  TTreeReaderArray<float> jet_cal_eta = {fReader, "jet_cal_eta"};
  TTreeReaderArray<float> jet_cal_phi = {fReader, "jet_cal_phi"};
  TTreeReaderArray<float> jet_cal_pt = {fReader, "jet_cal_pt"};
  TTreeReaderArray<float> jet_cal_e = {fReader, "jet_cal_e"};
  TTreeReaderArray<bool> jet_cal_isSTDOR = {fReader, "jet_cal_isSTDOR"};
  TTreeReaderValue<Float_t> MET = {fReader, "MET"};

  //Weight
  TTreeReaderValue<Int_t> dsid = {fReader, "dsid"};
  TTreeReaderValue<Float_t> sumWeightPRW = {fReader, "sumWeightPRW"};
  TTreeReaderValue<Float_t> weight = {fReader, "weight"};
  TTreeReaderValue<Float_t> kFactor = {fReader, "kFactor"};
  TTreeReaderValue<Float_t> filterEff = {fReader, "filterEff"};
  TTreeReaderValue<Float_t> puWeight = {fReader, "puWeight"};
  TTreeReaderValue<Float_t> mcWeight = {fReader, "mcWeight"};
  TTreeReaderValue<Float_t> amiXsection = {fReader, "amiXsection"};

  //Truth Study
  TTreeReaderArray<int> truthPdgId = {fReader, "truthPdgId"};
  TTreeReaderArray<float> truthEta = {fReader, "truthEta"};
  TTreeReaderArray<float> truthPhi = {fReader, "truthPhi"};
  TTreeReaderArray<float> truthPt = {fReader, "truthPt"};
  TTreeReaderArray<float> truthE = {fReader, "truthE"};


  //Weigght matrix
  map <int, map<string,float>> sumw_HAHM = { {508929,{{"a", 1.00003}, {"d", 1.0026}, {"e", 1.00007}}},
					     {508930,{{"a", 0.999796}, {"d", 1.00005}, {"e", 1.00007}}},
					     {508931,{{"a", 0.999811}, {"d", 0.661783}, {"e", 1.00022}}},
					     {508932,{{"a", 0.999812}, {"d", 0.894607}, {"e", 1.00007}}},
					     {507580,{{"a", 0.999891}, {"d", 0.999801}, {"e", 0.999933}}},   
					     {507581,{{"a", 0.999812}, {"d", 0.999694}, {"e", 0.999934}}},
					     {507582,{{"a", 0.999812}, {"d", 0.999694}, {"e", 0.999934}}}  };
  map < int, map<string,float>> JZwithSW = {{364700,{{"a",155738576.0},{"d",19976000.0},{"e",105629200.0}}},
					    {364701,{{"a",15982200.0},{"d", 19963000.0},{"e", 55951200.0}}},
					    {364702,{{"a",9609.11425781},{"d",7317.59912109},{"e",9589.80859375}}},
					    {364703,{{"a",115.947219849},{"d",145.456787109},{"e",189.096206665}}},
					    {364704,{{"a",5.31719923019},{"d",4.45149898529},{"e",5.79019641876}}},
					    {364705,{{"a",0.133524268866},{"d",0.166289106011},{"e",0.233590602875}}},
					    {364706,{{"a",0.0222406610847},{"d",0.0277969948947},{"e",0.03893109411}}},
					    {364707,{{"a",0.00305932993069},{"d",0.00382369011641},{"e",0.00535662751645}}},
					    {364708,{{"a",0.000884311739355},{"d",0.00110877072439},{"e",0.00154999201186}}},
					    {364709,{{"a",0.000171866515302},{"d",0.000214897969272},{"e",0.00027486376348}}},
					    {364710,{{"a",2.00281865546e-05},{"d",2.6503468689e-05},{"e",3.20957624353e-05}}},
					    {364711,{{"a",1.05570679807e-05},{"d",1.40227293741e-05},{"e",1.69650520547e-05}}},
					    {364712,{{"a",6.16185297986e-06},{"d",8.15005114418e-06},{"e",9.86921895674e-06}}}};
    //Weight Constants
      float m_DP = 0;
      float ctauRef = 0;
      float xSec = 0;
      float BranchingRatio = 0;
      float intLumi = 0;
      float intLumi2015 = 0;
      float intLumi2016 = 0;
      float intLumi2017 = 0;
      float intLumi2018 = 0;

      bool isFRVZ = true;
      bool isHAHM = true;


 // For microT 
    TTree * microT_00 = 0;
    //vector<bool> u_isMC;
    //vector<float> u_LJmuon_isLeading;
    vector<float> u_LJmuon_eta;
    vector<float> u_LJmuon_phi;
    vector<float> u_LJmuon_pt;
    //vector<float> u_LJmuon_z0;
    vector<float> u_LJmuon_RPCtime;
    //vector<int>   u_LJmuon_RPCinfo;
    //vector<float>   u_LJmuon_NNscore;
    vector<int>   u_LJmuon_author;
    vector<int>   u_LJmuon_type;

  //Options passed by Run.py
  bool doValidTrees;
  TString doCalRatioSyst;
  TString doTruthHiggsPtReweighting;
  bool doLiRe = false;
  
  // User defined variables
  TFile *outFile = nullptr;
  //Counters
  int maxEvents= 0; //Maximum iterations

  int eventCounter = 0;
  int isTriggeredCounter = 0;
  int TriggerHLTemuCounter = 0;
  int TriggerQCDcounter = 0;
  int PVcounter = 0;
  int notVHVBFcounter = 0;
  int TwoLJcounter = 0;
  int LJmuonCounter = 0;
  int LJjetCounter = 0;
  int nTotalEntries = 0;
  float WeventCounter = 0;
  float WisTriggeredCounter = 0;
  float WTriggerHLTemuCounter = 0;
  float WTriggerQCDcounter = 0;
  float WPVcounter = 0;
  float WnotVHVBFcounter = 0;
  float WTwoLJcounter = 0;
  float WLJmuonCounter = 0;
  float WLJjetCounter = 0;


  //Contadores Channels
  int mumuInitial = 0;
  int mumuMuonEta = 0;
  int mumuNoCB = 0;
  int mumuDNN = 0;
  float WmumuInitial = 0;
  float WmumuMuonEta = 0;
  float WmumuNoCB = 0;
  float WmumuDNN = 0;

  int hmuInitial = 0;
  int hmuMuonEta = 0;
  int hmuNoCB = 0;
  int hmuGapRatio = 0;
  int hmuTiming = 0;
  int hmuBIBtagger = 0;
  int hmuDNN = 0;
  int hmuDeltaPhi = 0;
  float WhmuInitial = 0;
  float WhmuMuonEta = 0;
  float WhmuNoCB = 0;
  float WhmuGapRatio = 0;
  float WhmuTiming = 0;
  float WhmuBIBtagger = 0;
  float WhmuDNN = 0;
  float WhmuDeltaPhi = 0;

  int hhInitial = 0;
  int hhGapRatio = 0;
  int hhTiming =  0;
  int hhBIBtagger = 0;
  int hhDeltaPhi = 0;
  int hhJVT = 0;
  float WhhInitial = 0;
  float WhhGapRatio = 0;
  float WhhTiming =  0;
  float WhhBIBtagger = 0;
  float WhhDeltaPhi = 0;
  float WhhJVT = 0;
  //Bool channels
  bool cutflow_00 = false;
  bool cutflow_02 = false;
  bool cutflow_22 = false;

  float sumw_a=0.;
  float sumw_d=0.;
  float sumw_e=0.;

  float farDPhi = 0;
  float ptImbalance;

  float evntWeight =0;
  float testWeight = 0;

  //Triggers
  bool t_3mu6_msonly = false;
  bool t_calRatio = false;
  bool t_nscan = false;

  //Bools cortes
  // Cuts for 0-0 channel
  bool etaCutLead = true;
  bool etaCutFar  = true;
  bool noCBcutLead = true;
  bool noCBcutFar  = true;
  bool DNNcutLead = true;
  bool DNNcutFar = true;
  bool RPCtimingCutLead = true;
  bool RPCtimingCutFar = true;
  float DNNscoreLead = 0;
  float DNNscoreFar = 0;

  // Cuts for 2-2 channel
  bool gapCutLead = true;
  bool gapCutFar  = true;
  bool timingCutLead = true;
  bool timingCutFar  = true;
  bool JVTcutLead = true;
  bool JVTcutFar = true;
  bool fMaxcutLead = true;
  bool fMaxcutFar = true;
  bool deltaTimingCut  = true;
  bool farDPhiCut  = true;
  bool BIBflagCutLead = true;
  bool BIBflagCutFar  = true;  
  bool BIBtaggerCutLead = true;
  bool BIBtaggerCutFar = true;
  float JVTscoreLead = 0.;
  float JVTscoreFar = 0.;
  float fMaxLead = 0.;
  float fMaxFar = 0.;
  float BIBtaggerScoreLead = 0.;
  float BIBtaggerScoreFar = 0.;
  float DPJtaggerScoreLead = 0.;
  float DPJtaggerScoreFar = 0.;
  float DPJtaggerScoreGlobal = -100.;
  float farDTiming = 0.;

  float   gapLead;
  float   gapFar;
  float   BIBflagLead;
  float   BIBflagFar;
  float   timingLead;
  float   timingFar;
  float   EMfracLead;
  float   EMfracFar;

  //Histogramas para plot de significancia en funcion de cortes 
  //2 hDPJ
  TH1D* CutEvents22 = nullptr;
  TH1D* CutWEvents22 = nullptr;
  TH1D* histMET22 = nullptr;
  TH1D* histDeltaPhi22 = nullptr;
  TH1D* histGapRatio22 = nullptr;
  TH1D* histTiming22 = nullptr;
  TH1D* histBIBtagger22 = nullptr;
  TH1D* histJVTtagger22 = nullptr;
  //2 muDPJ
  TH1D* CutEvents00 = nullptr;
  TH1D* CutWEvents00 = nullptr;
  TH1D* histMuonDNN00 = nullptr;
  //muDPJ + hDPJ
  TH1D* CutEvents02 = nullptr;
  TH1D* CutWEvents02 = nullptr;
  TH1D* histMuonEta02 = nullptr;
  TH1D* histGapRatio02 = nullptr;
  TH1D* histTiming02 = nullptr;
  TH1D* histBIBtagger02 = nullptr;
  TH1D* histMuonDNN02 = nullptr;
  TH1D* histDeltaPhi02 = nullptr;
  //truth
  TH1F* h_truthDP_pT = nullptr;
  float truthHiggsPt;
  TH1F* h_LJjet_pT = nullptr;
  TH1F* h_LJmuon_pT = nullptr;
  TH1F* h_LJjetRatio = nullptr;
  TH1F* h_LJmuonRatio = nullptr;
  //Sanity check
  TH1F* h_weight = nullptr;
  TH1F* h_EvntWeight = nullptr;
  TH1F* h_kfactor = nullptr;
  TH1F* h_filterEff = nullptr;
  TH1F* h_xSec = nullptr;
  TH1F* h_BranchingRatio = nullptr;
  TH1F* h_intLumi = nullptr;
  TH1F* h_GetSumW = nullptr;
  



  

  

  
  LJexplorer(TTree * /*tree*/ =0) { }
  virtual ~LJexplorer() { }
  virtual Int_t   Version() const { return 2; }
  virtual void    Begin(TTree *tree);
  virtual void    SlaveBegin(TTree *tree);
  virtual void    Init(TTree *tree);
  virtual Bool_t  Notify();
  virtual Bool_t  Process(Long64_t entry);
  virtual void    LJstudy();
  virtual void    SlaveTerminate();
  virtual void    Terminate();
  virtual bool    FindLeadingFarLJ();
  virtual void    FillMicroT00();
  virtual bool    CheckOrthogonalityVH();
  virtual bool    CheckOrthogonalityVBF();
  virtual bool    TriggerStudy();
  //virtual bool    TriggerStudyHLTemu();
  virtual bool    TriggerStudyQCD();
  virtual float   GetSumW();
  virtual void    FindAndCountChannels();
  virtual void    SetConstants();
  virtual void    FillHistograms();
  virtual void    TruthStudy();
  

  ClassDef(LJexplorer,0);

};

#endif

#ifdef LJexplorer_cxx




void LJexplorer::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the reader is initialized.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).
  std::cout<<"tree INIT: " << tree <<std::endl;
  fReader.SetTree(tree);

  

}

Bool_t LJexplorer::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void LJexplorer::SlaveBegin(TTree * /*tree*/)
{
  TString option = GetOption();

}

#endif // #ifdef LJexplorer_cxx
