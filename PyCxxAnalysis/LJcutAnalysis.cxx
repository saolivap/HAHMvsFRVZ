// dear Emacs, this is -*- c++ -*-
#define LJcutAnalysis_cxx
//#define FullNtuple
//#define hasClusters

#include "LJcutAnalysis.h"
#include <TH2.h>
#include <TStyle.h>
#include <TMath.h>
#include <TLorentzVector.h>
#include "TObject.h"
#include "TObjString.h"
#include "TString.h"

//void LJcutAnalysis::~LJcutAnalysis();
//    delete outFile;
//    delete microT_00;
  


void LJcutAnalysis::Begin(TTree *tree) {
    //Output file
    TString options = GetOption();
    TObjArray * optionsArr = options.Tokenize(" ");
    TString outFileName = ((TObjString*)(optionsArr->At(0)))->String();
    outFile = new TFile(outFileName, "Recreate");
    const char* nTotalEntriesChar = (((TObjString*)(optionsArr->At(1)))->String()).Data();
    nTotalEntries = atoi( nTotalEntriesChar );

    
    string cutflowMode( (string) ((((TObjString*)(optionsArr->At(2)))->String()).Data()) );
    std::cout << "cutflowMode: "<<  cutflowMode << "\n";
    
    //SetCutflowFlags(); Se puede implementar para personalizar que cutflows hacer
    // TEST bypass Init()
    fReader.SetTree(tree);
    std::cout<<"tree: " << tree << std::endl;
    //
    std::cout << "End of Begin" << std::endl;
}

Bool_t LJcutAnalysis::Process(Long64_t entry) {
    std::cout << "AQUI" << std::endl;

    fReader.SetLocalEntry(entry);
    eventCounter++;
    //u_eventNumber = *eventNumber; (no definido)
    if (eventCounter > 0 && eventCounter % 1000000 == 0){
        std::cout << "Processed " << eventCounter << " events" << std::endl;
    }

    //weight (273 Iacopo)
    //Cortes
     muonEtaCut = true;
    if (muonEtaCut) {
        std::cout << "SUCCESS" << std::endl;
    }
    LJstudy();

    if (muonEtaCut) FillMicroT00();


    return kTRUE;
}

void LJcutAnalysis::LJstudy() {
    //Reset flags
    std::cout << "antes" << std::endl;
    muonEtaCut = true;
    if (muonEtaCut) {
        std::cout << "SUCCESS" << std::endl;
    }
     // Cuts on single muonLJ
  for ( unsigned int m = 0; m < LJmuon_eta.GetSize() ; ++m ) {
    // Muon eta cut
    if ( ( types[*lead_index] != 2 ) &&
	 ( LJmuon_index[m] == *lead_index ) && 
	 ( TMath::Abs( LJmuon_eta[m] ) > 1. && TMath::Abs( LJmuon_eta[m] ) < 1.1 ) )   muonEtaCut = false;

    //Se llenan los trees de salida
    
    //if (muonEtaCut) FillMicroT00();
    
}
}
void LJcutAnalysis::SlaveTerminate() { }

void LJcutAnalysis::Terminate() {
    

    std::cout << "Se escriben los archivos de salilda" << std::endl;


    outFile->WriteObject(microT_00, "microTree");
    outFile->Close();
}

void LJcutAnalysis::FillMicroT00() {

    //if (!microT_00){
    microT_00 = new TTree("microT_00","Selection");
    microT_00->Branch("LJmuon_eta", &u_LJmuon_eta);
    microT_00->Branch("LJmuon_phi", &u_LJmuon_phi);
    microT_00->Branch("LJmuon_pt", &u_LJmuon_pt);
    microT_00->Branch("LJmuon_RPCtime", &u_LJmuon_RPCtime);
    microT_00->Branch("LJmuon_author", &u_LJmuon_author);
    microT_00->Branch("LJmuon_type", &u_LJmuon_type);
    //}
    vector<int> goodLJmuon;

    
    u_LJmuon_eta.clear();
    u_LJmuon_phi.clear();
    u_LJmuon_pt.clear();
    u_LJmuon_RPCtime.clear();
    u_LJmuon_author.clear();
    u_LJmuon_type.clear();

    /* Find LJ to be saved (hace distinción con lead y far index)
    Por ahora no esta implementado */
    for ( int j=0; j<(int)LJmuon_eta.GetSize() ; ++j){
        if ( ( LJmuon_index[j] != *lead_index ) && ( LJmuon_index[j] != *far_index ) ) continue;
        goodLJmuon.push_back(j);    
    }
    // Actual save
    //u_isMC.push_back(isMC);
    for ( int mu : goodLJmuon ){
      //u_eventNumber = *eventNumber;
      u_LJmuon_eta.push_back( LJmuon_eta[mu] );
      u_LJmuon_phi.push_back( LJmuon_phi[mu] );
      u_LJmuon_pt.push_back( LJmuon_pt[mu] );
      u_LJmuon_RPCtime.push_back( LJmuon_RPCtime[mu] );
      u_LJmuon_author.push_back( LJmuon_author[mu] );
      u_LJmuon_type.push_back( LJmuon_type[mu] );
    }
    
    microT_00->Fill();

}

void LJcutAnalysis::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the reader is initialized.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).
  std::cout<<"INIT"<<std::endl;
  fReader.SetTree(tree);
}

Bool_t LJcutAnalysis::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void LJcutAnalysis::SlaveBegin(TTree *tree)
{
  TString option = GetOption();
}


//#endif